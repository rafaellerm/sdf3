#include "pipe_reader.h"

#include <unistd.h>
#include <libxml/parser.h>

#include <iostream>
#include <string>
#include <cstring>

/**
xmlInputReadCallback
context:	an Input context
buffer:	the buffer to store data read
len:	the length of the buffer in bytes
Returns:	the number of bytes read or -1 in case of error
*/
int	stdin_read_func(void * context,
                char * buffer,
                int len)
{
    static char *int_buff = NULL;
    static int buff_size = 0;
    static size_t used_buff = 0;

    const static char end_tag[] = "</sdf3>";
    const static size_t end_tag_length = sizeof(end_tag) -1;
    static ssize_t end_state = 0;
    static bool eof = false;

    if(eof)
    {
    	eof = false;
    	end_state = 0;
    	std::cout << "Ending file!";
    	return 0;
    }
    // Se ja havia um excedente antes
    if (used_buff > 0)
    {
        int retval = used_buff;
        used_buff = 0;
        strncpy(buffer, int_buff, retval);
        return retval;
    }

    // Preparar-se para o pior caso de tamanho de buffer
    if (buff_size < len)
    {
        if (int_buff != NULL)
            free(int_buff);
        int_buff = (char*) malloc(len);
        buff_size = len;
    }

    // Ler de stdin e escrever em buffer
    ssize_t read_bytes;
    read_bytes = read(STDIN_FILENO, buffer, len);

    //DEBUG
    memset(int_buff, 0, buff_size);

    for (size_t pos; pos < read_bytes; ++pos)
    {
    	if (buffer[pos] == end_tag[end_state])
    		++end_state;
    	else
    		end_state = 0;
    	if (end_state == end_tag_length)
    	{
    		// Fim do arquivo, salvar e retornar
    		used_buff = read_bytes - pos;
    		pos++;
    		strncpy(int_buff, buffer+pos, used_buff);
    		puts("End reached!\n\tEnd:");
    		fwrite(buffer, 1, pos, stdout);
    		puts("\tExedent:\n");
    		fwrite(int_buff, 1, used_buff, stdout);
    		eof = true;
    		return pos;
    	}
    }
    return read_bytes;
    // Verificar se o buffer leu demais
//    char *end_point;
//    end_point = strstr(buffer, end_tag);
//    if(end_point == NULL || (end_point > buffer + read_bytes)) // Se ainda nao chegou no fim do xml
//        return read_bytes;
//
//    // Se leu demais, remover o que passou
//    char* message_end = (end_point + sizeof(end_tag));
//    int excess_chars = read_bytes - (message_end - buffer);
//
//    // Salvar o excedente
//    strncpy(int_buff, message_end, excess_chars);
//    used_buff = excess_chars;
//
//    std::cerr << "Remaining:\n"<<buffer<<std::endl;
//    // Just for safety
//    *message_end = 0;
//    return message_end - buffer;
}

int main(int argc, char **argv)
{
//    xmlTextReaderPtr reader;
//    int ret_st;
    xmlDocPtr doc;
    std::string rest;
    size_t i=0;
    do
    {
    	std::cout << "Started read " << ++i << std::endl;
//        doc = xmlReadFd(STDIN_FILENO, NULL, NULL, 0);
    	doc = xmlReadIO(stdin_read_func, NULL, NULL, NULL, NULL, 0);
        if(doc == NULL)
        {
            std::cout << "Failed to parse " <<i<< std::endl;
        } else {
            std::cout << "Read successfully " << i << std::endl;
        }
//        std::cout << "Test" <<std::endl;
//        std::cin >> rest;
//        std::cout << "Rest (" << rest.size() << "): " << rest << std::endl;
        xmlFreeDoc(doc);
    } while(doc != NULL);

//    std::cin >> rest;
//    std::cout << "Rest (" << rest.size() << "): " << rest << std::endl;

    xmlCleanupParser();

//    char data[100];
//    int n_read;
//
//    do
//    {
//        n_read = read(fd[READ], data, sizeof(data));
//        std::cout << n_read << ": " << data << std::endl;
//    } while ()
}
