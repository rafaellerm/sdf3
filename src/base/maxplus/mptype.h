/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   mptype.h
 *
 *  Author          :   Marc Geilen (m.c.w.geilen@tue.nl)
 *
 *  Date            :   March 23, 2009
 *
 *  Function        :   maxplus type definitions and operations
 *
 *  History         :
 *      23-03-09    :   Initial version.
 *
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#ifndef BASE_MAXPLUS_MPTYPE_H_INCLUDED
#define BASE_MAXPLUS_MPTYPE_H_INCLUDED

#include "../basic_types.h"

// MaxPlus infinity
#define MP_MINUSINFINITY            -1000000.0
#define MP_ISMINUSINFINITY(a)       (a<-900000.0)

// MaxPlus addition
#define MP_PLUS(a,b) ((MP_ISMINUSINFINITY(a) || MP_ISMINUSINFINITY(b)) ? \
                      MP_MINUSINFINITY : a + b)

// MaxPlus epsilon (used to compare floating point numbers for equality)
#define MP_EPSILON (1e-10)

namespace MaxPlus 
{

typedef CDouble MPThroughput;
typedef CDouble MPDelay;
typedef CDouble MPTime;

}

#endif
