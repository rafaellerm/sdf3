/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   mpmatrix.h
 *
 *  Author          :   Marc Geilen (m.c.w.geilen@tue.nl)
 *
 *  Date            :   March 23, 2009
 *
 *  Function        :   MaxPlus matrices
 *
 *  History         :
 *      23-03-09    :   Initial version.
 *
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#ifndef MPMATRIX_H_INCLUDED
#define MPMATRIX_H_INCLUDED

#include "mptype.h"
#include <vector>

namespace MaxPlus {

	/****************************************************
	* represents a MaxPlus column vector
	****************************************************/
	class Vector {
	public:
		Vector(unsigned int size = 0);
		Vector(std::vector<MPTime>* v);
		~Vector();
		unsigned int getSize() const;
		MPTime get(unsigned int row) const;
		void put(unsigned int row, MPTime value);
	private:
		vector<MPTime> table;
	};

	/****************************************************
	* represents a square MaxPlus matrix
	****************************************************/
	class Matrix {
	public:
		Matrix(unsigned int size);
		~Matrix();
		unsigned int getSize() const;
		MPTime get(unsigned int row, unsigned int column) const;
		void put(unsigned int row, unsigned int column, MPTime value);
		Vector* mpmultiply(const Vector& v);
	private:
		vector<MPTime> table;
	};

}

#endif
