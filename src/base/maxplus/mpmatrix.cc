/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   mpmatrix.cc
 *
 *  Author          :   Marc Geilen (m.c.w.geilen@tue.nl)
 *
 *  Date            :   March 23, 2009
 *
 *  Function        :   MaxPlus vectors and matrices
 *
 *  History         :
 *      23-03-09    :   Initial version.
 *
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include <stdlib.h>
#include "mpmatrix.h"
#include "../exception/exception.h"

#define MAX(a,b) ((a) > (b) ? (a) : (b))

namespace MaxPlus {

/**
 * Vector()
 * Construct a Maxplus vector of size.
 */
Vector::Vector(unsigned int size)
{
    table.resize(size);
    for (uint i = 0; i < size; i++)
        table[i] = 0;
}

/**
 * Vector()
 * Construct a Maxplus vector from an std:vector.
 */
Vector::Vector(std::vector<MPTime>* v) 
{
    table.resize(v->size());
    for (uint i = 0; i < v->size(); i++)
        table[i] = v->at(i);
}

/**
 * ~Vector()
 * Destructor of MaxPlus vector.
 */
Vector::~Vector()
{
}

/**
 * getSize()
 * Get size of the vector.
 */
unsigned int Vector::getSize(void) const 
{
    return table.size();
}

/**
 * get()
 * Get an entry from the vector. Row index must be between 0 and size-1.
 */
MPTime Vector::get(unsigned int row) const
{
    return table[row];
}

/**
 * put()
 * Put a value at the specified row. Row index must be between 0 and size-1.
 */
void Vector::put(unsigned int row, MPTime value)
{
    if (table.size() <= row)
        table.resize(row+1, 0);
        
    table[row] = value;
}

/**
 * Matrix()
 * Construct a Maxplus matrix of size by size.
 */
Matrix::Matrix(unsigned int size)
{
    table.resize(size*size);
}

/**
 * ~Matrix()
 * Destructor of MaxPlus matrix.
 */
Matrix::~Matrix()
{
}

/**
 * getSize()
 * Get size of the matrix.
 */
unsigned int Matrix::getSize(void) const
{
    return table.size();
}

/**
 * get()
 * Get an entry from the matrix. Row and column index must be between 0 and 
 * size-1.
 */
MPTime Matrix::get(unsigned int row, unsigned int column) const
{
    return this->table[row*getSize()+column];
}

/**
 * put()
 * Put a value in the matrix. Row and column index must be between 0 and size-1.
 */
void Matrix::put(unsigned int row, unsigned int column, MPTime value)
{
    table[row*getSize()+column] = value;
}

/**
 * mpmultiply()
 * Matrix multiplication.
 */
Vector* Matrix::mpmultiply(const Vector& v)
{
    // Check size of the matrix and vector
    if(getSize() != v.getSize())
    { 
        throw CException("Matrix and vector are of unequal size in"
                         "Matrix::mpmultiply");
    }

    // Allocate space of the resulting vector
    Vector* res = new Vector(getSize());
    
    // Perform point-wise multiplication
    for(unsigned int i=0; i<getSize(); i++)
    {
        MPTime m = MP_MINUSINFINITY;
        for(unsigned int k=0; k<getSize(); k++)
        {
            m = MAX(m, MP_PLUS(get(i,k), v.get(k)));
        }
        res->put(i,m);
    }

    return res;
}

}

