/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   sdf3analyze.cc
 *
 *  Author          :   Sander Stuijk (s.stuijk@tue.nl)
 *
 *  Date            :   21 April 2009
 *
 *  Function        :   FSM-based SADF Graph Analysis Functionality
 *
 *  History         :
 *      21-04-09    :   Initial version.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "sdf3analyze.h"
#include "../../../base/base.h"
#include "../../../sdf/sdf.h"
#include "../../fsmsadf.h"

using namespace FSMSADF::MPExplore;

namespace FSMSADF
{

/**
 * helpMessage ()
 * Function prints help message for the tool.
 */
void ToolAnalyze::helpMessage(ostream &out)
{
    out << "SDF3 " << TOOL << " (version " << DOTTED_VERSION ")" << endl;
    out << endl;
    out << "Usage: " << TOOL << " --graph <file> --check <property>";
    out << " [--output <file>]" << endl;
    out << "   --graph  <file>     input SADF graph" << endl;
    out << "   --output <file>     output file (default: stdout)" << endl;
    out << "   --algo <algorithm>  analyze the graph with requested algorithm:";
    out << endl;
    out << "       extractscenariographs" << endl;
    out << "       throughput" << endl;
    out << "       isolatescenarios" << endl;
    out << "       mpeigen" << endl;
    out << "       throughput-statespace" << endl;
}

/**
 * AnalyzeGraph()
 * The function that calls the actual analysis functions
 */
void ToolAnalyze::AnalyzeGraph(ostream &out)
{
    Graph *fsmsadfGraph;
    CNode *applicationNode;
    CTimer timer;

    // Find sdf graph in XML structure
    applicationNode = settings.xmlAppGraph;

    // Construction SDF graph model
    fsmsadfGraph = new Graph();
    fsmsadfGraph->constructFromXML(applicationNode);

    CPairs &analyze = settings.arguments;
    
    // Select the correct algorithm
    if (analyze.front().key == "extractscenariographs")
    {
        // Print the graph which has just been loaded from file
        fsmsadfGraph->print(cout);
        
        // Create XML
        CNode *sdf3Node = CNewNode("sdf3");
        CAddAttribute(sdf3Node, "version", "1.0");
        CAddAttribute(sdf3Node, "type", "fsmsadf");
        CAddAttribute(sdf3Node, "xmlns:xsi",
                                "http://www.w3.org/2001/XMLSchema-instance");
        CAddAttribute(sdf3Node, "xsi:noNamespaceSchemaLocation",
                                "http://www.es.ele.tue.nl/sdf3/xsd/sdf3-fsmsadf.xsd");
        
        // Application graph node
        CNode *appNode = CAddNode(sdf3Node, "applicationGraph");
       
        // SDF node
        fsmsadfGraph->convertToXML(appNode);
        
        // Create document and save it
        CDoc *doc = CNewDoc(sdf3Node);
        CSaveFile(out, doc);
        
        Scenarios scenarios = fsmsadfGraph->getScenarios();
        for (Scenarios::const_iterator i = scenarios.begin();
                i != scenarios.end(); i++)
        {
            TimedSDFgraph *g = fsmsadfGraph->extractSDFgraph(*i);
            outputSDFasXML(g, out);
        }
    }
    else if (analyze.front().key == "throughput")
    {
        Throughput thr;

        // Measure execution time
        startTimer(&timer);
        
        thr = throughputAnalysisGraph(fsmsadfGraph);
        cout << "Throughput (scenario graph):       " << thr << endl;

        // Measure execution time
        stopTimer(&timer);
        out << "analysis time: ";
        printTimer(out, &timer);
        out << endl;

        // Measure execution time
        startTimer(&timer);

        thr = throughputAnalysisUsingRefSchedule(fsmsadfGraph);
        cout << "Throughput (reference schedule):   " << thr << endl;

        // Measure execution time
        stopTimer(&timer);
        out << "analysis time: ";
        printTimer(out, &timer);
        out << endl;

        // Measure execution time
        startTimer(&timer);

        thr = throughputAnalysisWithScenarioTransitions(fsmsadfGraph);
        cout << "Throughput (scenario transitions): " << thr << endl;

        // Measure execution time
        stopTimer(&timer);
        out << "analysis time: ";
        printTimer(out, &timer);
        out << endl;
    }
    else if (analyze.front().key == "isolatescenarios")
    {
        Graph *newFSMsadfGraph = fsmsadfGraph->clone(GraphComponent(NULL, 0));
        
        // Create a cloned copy of the graph
        newFSMsadfGraph->isolateScenarios();

        // Create XML
        CNode *sdf3Node = CNewNode("sdf3");
        CAddAttribute(sdf3Node, "version", "1.0");
        CAddAttribute(sdf3Node, "type", "fsmsadf");
        CAddAttribute(sdf3Node, "xmlns:xsi",
                                "http://www.w3.org/2001/XMLSchema-instance");
        CAddAttribute(sdf3Node, "xsi:noNamespaceSchemaLocation",
                                "http://www.es.ele.tue.nl/sdf3/xsd/sdf3-fsmsadf.xsd");
        
        // Application graph node
        newFSMsadfGraph->convertToXML(CAddNode(sdf3Node, "applicationGraph"));
        
        // Create document and save it
        CDoc *doc = CNewDoc(sdf3Node);
        CSaveFile(out, doc);
        
        delete newFSMsadfGraph;
    }
    else if (analyze.front().key == "mpeigen")
    {
        map<Scenario*, MaxPlus::Vector*> eigenvector;
        map<Scenario*, MPTime> period;
        map<FSMtransition*, MPTime> delay;
        map<Scenario*, Exploration*> mpe;
        Throughput thr;
        
        // The set of scenarios...
        Scenarios scenarios = fsmsadfGraph->getScenarios();

        // Create Max-Plus exploration object for each scenario
        for (Scenarios::const_iterator i = scenarios.begin();
                i != scenarios.end(); i++)
        {
            Scenario *s = *i;
            mpe[s] = new Exploration;
            mpe[s]->G = new SGraph(s->getScenarioGraph(), s);
        }
        
        // Compute eigenvector and period for each scenario
        for (Scenarios::const_iterator i = scenarios.begin();
                i != scenarios.end(); i++)
        {
            Scenario *s = *i;
            MaxPlus::Vector *v_s = NULL;
            MPTime T_s;

            // Compute eigenvector and period of this scenario
            mpe[s]->exploreEigen(&v_s, &T_s);
            
            // Store results
            eigenvector[s] = v_s;
            period[s] = T_s;
            
            // Output result
            cout << "eigenvalue: " << T_s << endl;
            for (uint x = 0; x < v_s->getSize(); x++)
                cout << v_s->get(x) << " ; ";
            cout << endl;
        }

        // Cleanup
        for (map<Scenario*, MaxPlus::Vector*>::iterator i = eigenvector.begin();
                i != eigenvector.end(); i++)
        {
            delete i->second;
        }
        for (map<Scenario*, Exploration*>::iterator i = mpe.begin();
                i != mpe.end(); i++)
        {
            delete i->second->G;
            delete i->second;
        }
    }
    else if (analyze.front().key == "throughput-statespace")
    {
        Throughput thr;

        // Measure execution time
        startTimer(&timer);
        
        thr = throughputAnalysisGraphUsingStateSpace(fsmsadfGraph);
        cout << "Throughput (state-space):          " << thr << endl;

        // Measure execution time
        stopTimer(&timer);
        out << "analysis time: ";
        printTimer(out, &timer);
        out << endl;
    }
    else
    {
        throw CException("Unknown algorithm.");
    }
}

} // end namespace

/**
 * main ()
 * It does none of the hard work, but it is very needed...
 */
int main(int argc, char **argv)
{
    FSMSADF::ToolAnalyze T;

    int exit_status = 0;
    ofstream out;

    try
    {
        // Initialize the program
        T.initSettings(MODULE, argc, argv);

        // Set output stream
        if (!T.settings.outputFile.empty())
            out.open(T.settings.outputFile.c_str());
        else
            ((ostream&)(out)).rdbuf(cout.rdbuf());

        // Perform the actual function
        T.AnalyzeGraph(out);
    }
    catch (CException &e)
    {
        cerr << e;
        exit_status = 1;
    }

    return exit_status;
}

