/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   binding_aware_sdfg.cc
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   May 14, 2009
 *
 *  Function        :   Binding-aware SDFG
 *
 *  History         :
 *      14-05-09    :   Initial version.
 *
 * $Id: binding_aware_graph.cc,v 1.1 2009-12-23 13:37:21 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "binding_aware_graph.h"

namespace FSMSADF
{

/**
 * actorMappedToProcessor()
 * The function returns true when the supplied actor is mapped to a 
 * processor. Otherwise the function returns false.
 */
bool BindingAwareGraph::BindingAwareProperties::actorMappedToProcessor(
        Actor *a) const
{
    if (actorToProcessor.find(a) != actorToProcessor.end())
        return true;
    return false;
}

/**
 * channelMappedToConnection()
 * The function returns true when the supplied channel is mapped to a 
 * connection. Otherwise the function returns false.
 */
bool BindingAwareGraph::BindingAwareProperties::channelMappedToConnection(
        Channel *c) const
{
    if (channelToConnection.find(c) != channelToConnection.end())
        return true;
    return false;
}

/**
 * BindingAwareGraph()
 * Constructor.
 */
BindingAwareGraph::BindingAwareGraph(PlatformBinding *b, 
        const FlowType flowType)
{
    // Create a binding-aware graph
    constructBindingAwareGraph(b, flowType);
}

/**
 * ~BindingAwareGraph()
 * Destructor.
 */
BindingAwareGraph::~BindingAwareGraph()
{
    delete bindingAwareGraph;
}

/**
 * getScenarioOfScenarioGraph()
 * The function returns the scenario which uses the supplied scenario graph.
 * It is guaranteed that there is only one scenario which uses a scenario graph,
 * since the binding-aware graph uses a graph with isolated scenario.
 */
Scenario *BindingAwareGraph::getScenarioOfScenarioGraph(
        const ScenarioGraph *sg) const
{
    Graph *g = sg->getGraph();
    
    // Iterate over all scenarios
    for (Scenarios::iterator i = g->getScenarios().begin();
            i != g->getScenarios().end(); i++)
    {
        Scenario *s = *i;
        
        if (s->getScenarioGraph() == sg)
            return s;
    }
    
    throw CException("Scenario graph is not used in any scenario.");
}

/**
 * extractActorMapping ()
 * The function extracts the actor binding and scheduling from the platform
 * graph.
 */
void BindingAwareGraph::extractActorMapping(PlatformBinding *b)
{
    // Iterate over the tile bindings
    for (TileBindings::iterator i = b->getTileBindings().begin();
            i != b->getTileBindings().end(); i++)
    {
        TileBinding *tb = *i;
        
        // Iterate over the processor bindings
        for (ProcessorBindings::iterator j = tb->getProcessorBindings().begin();
                j != tb->getProcessorBindings().end(); j++)
        {
            ProcessorBinding *pb = *j;
            
            // Iterate over the actor bindings
            for (map<Scenario*, Actors>::iterator k = 
                    pb->getActorBindings().begin();
                    k != pb->getActorBindings().end(); k++)
            {
                Scenario *s = k->first;
                Actors &actors = k->second;
                
                // Find scenario in the binding-aware graph which
                // corresponds to scenario s.
                Scenario *sn = bindingAwareGraph->getScenario(s->getName());
                
                // Scenario graph in the binding-aware graph
                ScenarioGraph *sg = sn->getScenarioGraph();
                
                // Iterate over all actors
                for (Actors::iterator l = actors.begin(); 
                        l != actors.end(); l++)
                {
                    Actor *a = *l;
                    
                    // Find corresponding actor in sg
                    Actor *an = sg->getActor(a->getName());
                    
                    // Store binding of actor an to processor p in the scenario
                    // graph sg
                    bindingAwareProperties[sg].actorToProcessor[an] 
                                                           = pb->getProcessor();
                }
            }
            
            // Iterate over the static order schedules
            for (map<Scenario*, StaticOrderSchedule>::iterator k =
                    pb->getStaticOrderSchedules().begin();
                    k != pb->getStaticOrderSchedules().end(); k++)
            {
                Scenario *s = k->first;
                StaticOrderSchedule *so = &(k->second);
                
                // Find scenario in the binding-aware graph which
                // corresponds to scenario s.
                Scenario *sn = bindingAwareGraph->getScenario(s->getName());
                
                // Scenario graph in the binding-aware graph
                ScenarioGraph *sg = sn->getScenarioGraph();
                
                // Store binding of static-order schedule so to processor p 
                // in the scenario graph sg
                bindingAwareProperties[sg].
                        processorToStaticOrderSchedule[pb->getProcessor()] = so;
            }
            
            // Iterate over the TDMA resource allocations
            for (map<Scenario*, Time>::iterator k =
                    pb->getWheelsizeAllocations().begin();
                    k != pb->getWheelsizeAllocations().end(); k++)
            {
                Scenario *s = k->first;
                Time t = k->second;
                
                // Find scenario in the binding-aware graph which
                // corresponds to scenario s.
                Scenario *sn = bindingAwareGraph->getScenario(s->getName());
                
                // Scenario graph in the binding-aware graph
                ScenarioGraph *sg = sn->getScenarioGraph();
                
                // Store binding of time slice t to processor p 
                // in the scenario graph sg
                bindingAwareProperties[sg].
                                   processorToTimeslice[pb->getProcessor()] = t;
            }
        }
    }
}

/**
 * extractChannelMapping ()
 * The function extracts the channel binding and scheduling from the platform
 * graph.
 */
void BindingAwareGraph::extractChannelMapping(PlatformBinding *b)
{
    // Iterate over the connection bindings
    for (ConnectionBindings::iterator i = b->getConnectionBindings().begin();
            i != b->getConnectionBindings().end(); i++)
    {
        ConnectionBinding *cb = *i;
        
        // Iterate over all channel bindings
        for (map<Scenario*, Channels>::iterator j = 
                cb->getChannelBindings().begin();
                j != cb->getChannelBindings().end(); j++)
        {
            Scenario *s = j->first;
            Channels &channels = j->second;
            
            // Find scenario in the binding-aware graph which
            // corresponds to scenario s.
            Scenario *sn = bindingAwareGraph->getScenario(s->getName());
            
            // Scenario graph in the binding-aware graph
            ScenarioGraph *sg = sn->getScenarioGraph();

            // Iterate over all channels
            for (Channels::iterator k = channels.begin();
                    k != channels.end(); k++)
            {
                Channel *c = *k;
                
                // Find corresponding channel in sg
                Channel *cn = sg->getChannel(c->getName());

                // Store binding of channel cn to connection c in the scenario
                // graph sg
                bindingAwareProperties[sg].channelToConnection[cn]
                                                       = cb->getConnection();
            }
        }
    }
}

/**
 * extractGraphBindingConstraints()
 * Extract the graph binding constraints from the platform binding.
 */
void BindingAwareGraph::extractGraphBindingConstraints(PlatformBinding *b)
{
    graphBindingConstraints = b->getGraphBindingConstraints()
                                                    ->clone(bindingAwareGraph);
}

/**
 * constructBindingAwareGraph ()
 * Create a binding-aware graph for the application graph taking into account
 * the properties of the platform graph and the mapping as specified in the
 * platform binding.
 */
void BindingAwareGraph::constructBindingAwareGraph(PlatformBinding *b, 
        const FlowType flowType)
{
    // Create a clone of the application graph. This clone is the basis for 
    // the binding-aware graph. It will be extended later with additional
    // actors and edges to model binding decisions.
    bindingAwareGraph = b->getApplicationGraph()->clone(GraphComponent(NULL, 0));

    // Are the scenarios in the binding aware graph isolated?
    if (!bindingAwareGraph->hasIsolatedScenarios())
        throw CException("Binding-aware graph does not have isolated "
                         "scenarios.");

    // Extract actor related binding and scheduling properties
    extractActorMapping(b);
    
    // Extract channel related binding and scheduling properties
    extractChannelMapping(b);
    
    // Extract graph binding constraints
    extractGraphBindingConstraints(b);
    
    // Construct the graph
    switch (flowType)
    {
        case FlowTypeNSoC:
            modelBindingInNSoCFlow();
            break;

        default:
            throw CException("Specified flow type is not supported in "
                             "binding-aware graph.");
            break;
    }
    
    // Cleanup
    delete graphBindingConstraints;
}

/**
 * createMappedActorNSoC ()
 * The function models an actor mapped to a processor.
 */
void BindingAwareGraph::createMappedActorNSoC(Actor *a, Processor *p, 
        Scenario *s)
{
    Channel *c;
    
    // Set the execution time of actor a on processor p as its worst-case 
    // response time
    a->setExecutionTimeOfScenario(s, "wcrt", 
                                a->getExecutionTimeOfScenario(s, p->getType()));
    a->setDefaultProcessorType("wcrt");

    // Add self-loop with one token
    c = a->getScenarioGraph()->createChannel(a, a);
    c->setName(a->getName() + "-selfedge");
    c->getSrcPort()->setRateOfScenario(s, 1);
    c->getDstPort()->setRateOfScenario(s, 1);
    c->setInitialTokens(1);
}

/**
 * createMappedChannelToTileNSoC ()
 * The function models a channel mapped to a tile.
 */
void BindingAwareGraph::createMappedChannelToTileNSoC(Channel *c, 
        ChannelBindingConstraints *bc, Scenario *s)
{
    BufferSize b = bc->getBufferSize();
    Channel *m;
        
    // Add channel from dst to src actor to model memory space
    m = c->getScenarioGraph()->createChannel(c->getDstActor(),c->getSrcActor());
    m->setName(c->getName() + "-buffer");
    m->getSrcPort()->setRateOfScenario(s,c->getDstPort()->getRateOfScenario(s));
    m->getDstPort()->setRateOfScenario(s,c->getSrcPort()->getRateOfScenario(s));
    
    // Initial tokens reflect storage space
    if (b[BufferSize::Mem] < c->getInitialTokens())
        throw CException("Insufficient buffer space to store initial tokens.");
    m->setInitialTokens(b[BufferSize::Mem] - c->getInitialTokens());
}

/**
 * createMappedChannelToConnectionNSoC ()
 * The function models a channel mapped to a connection.
 *     
 * The transformation performed by this function on a channel c with n
 * initial tokens and a buffer size distribution b looks as follows
 *  
 *          p    c           n               q
 *        s -----------------*---------------> d
 *                      
 *                           |
 *                           V
 *                      
 *               c           n           
 *        +------------------*--------------------------+
 *        |                                             | 
 *      p | p   b[src]-n       b[dst]                 q v q
 *  +---- s <-----*------+ +------*-------------------- d <------+
 *  |   p | p  cs        | |1         cd              q ^ q      |
 *  |     |              | |                            |        |
 *  |     |           1 1| v 1      1      1 1    1     |        |
 *  |     +--------*---> acor ------> acol --> at ------+        |
 *  |         csr  n   1 | ^ 1  crl        clt      ctd          |
 *  |                    | |                                     |
 *  |                  1 * |                                     |
 *  |                    +-+                                     |
 *  |                    ca                                      |
 *  |      n    csd                1    1     cdd                |
 *  +------*-----------------------> ad -------------------------+
 *
 * Actor ad enforces the minimal delay of channel c to be considered in TDMA
 * allocation. This minimal delay creates scheduling freedom for the 
 * interconnect scheduling problem.
 *
 * Actors acor and acol models the latency and rrate of sending a token through 
 * the channel c. This is a latency-rate model of the connection. The assumption
 * is that buffer space is released in the source channel as soon as the data
 * is sent. At this moment, space is claimed at the destination side (i.e. a
 * transmission on the connection may only start when space is available at
 * the destination).
 *
 * Actor at models the worst-case TDMA timewheel synchronization which may
 * occur when tokens are sent from the source processor to the destination
 * processor.
 *
 * Channels cs and cd model the source and destination buffer of the channel.
 */
void BindingAwareGraph::createMappedChannelToConnectionNSoC(Channel *c, 
        ChannelBindingConstraints *bc, Connection *co, Scenario *s)
{
    // Scenario graph
    ScenarioGraph *sg = c->getScenarioGraph();

    // Buffer size of channel c
    BufferSize b = bc->getBufferSize();
    
    // Processor to which destination actor of channel c is bound
    Processor *pd = bindingAwareProperties[sg].actorToProcessor[c->getDstActor()];
    
    // Execution times of the actors acol, acor, ad, and al
    Time latency = co->getLatency();
    Time rate = (Time)(ceil(c->getTokenSizeOfScenario(s) / bc->getBandwidth()));
    Time tdma = pd->getWheelsize() 
                          - bindingAwareProperties[sg].processorToTimeslice[pd];
    Time delay = bc->getDelay();
    
    // Create an actor acol which models the connection latency
    Actor *acol = sg->createActor();
    acol->setName(c->getName() + "_latency");
    acol->setExecutionTimeOfScenario(s, "latency", latency);
    acol->setDefaultProcessorType("latency");

    // Create an actor acor which models the connection rate
    Actor *acor = sg->createActor();
    acor->setName(c->getName() + "_rate");
    acor->setExecutionTimeOfScenario(s, "rate", rate);
    acor->setDefaultProcessorType("rate");

    // Create actor ad which models the timewheel synchronization
    Actor *at = sg->createActor();
    at->setName(c->getName() + "_tdma");
    at->setExecutionTimeOfScenario(s, "tdma", tdma);
    at->setDefaultProcessorType("tdma");

    // Create actor ad which models the minimal delay of channel c
    Actor *ad = sg->createActor();
    ad->setName(c->getName() + "_delay");
    ad->setExecutionTimeOfScenario(s, "delay", delay);
    ad->setDefaultProcessorType("delay");

    // Create channel to model source buffer
    Channel *cs = sg->createChannel(acor, c->getSrcActor());
    cs->setName(c->getName() + "-src-buffer");
    cs->getSrcPort()->setRateOfScenario(s, 1);
    cs->getDstPort()->setRateOfScenario(s,c->getSrcPort()->getRateOfScenario(s));
    if (b[BufferSize::Src] < c->getInitialTokens())
        throw CException("Insufficient buffer space for initial tokens.");
    cs->setInitialTokens(b[BufferSize::Src] - c->getInitialTokens());

    // Create channel to model destination buffer
    Channel *cd = sg->createChannel(c->getDstActor(), acor);
    cd->setName(c->getName() + "-dst-buffer");
    cd->getSrcPort()->setRateOfScenario(s,c->getDstPort()->getRateOfScenario(s));
    cd->getDstPort()->setRateOfScenario(s, 1);
    cd->setInitialTokens(b[BufferSize::Dst]);

    // Create channel from source actor to actor which models connection rate
    Channel *csr = sg->createChannel(c->getSrcActor(), acor);
    csr->setName(c->getName() + "-connection-rate");
    csr->getSrcPort()->setRateOfScenario(s,c->getSrcPort()->getRateOfScenario(s));
    csr->getDstPort()->setRateOfScenario(s, 1);
    csr->setInitialTokens(c->getInitialTokens());

    // Add self-loop to actor acor (only 1 token sent at the same time)
    Channel *ca = sg->createChannel(acor, acor);
    ca->setName(c->getName() + "-self-edge");
    ca->getSrcPort()->setRateOfScenario(s, 1);
    ca->getDstPort()->setRateOfScenario(s, 1);
    ca->setInitialTokens(1);

    // Create channel from actor which models connection rate to actor which
    // models connection latency
    Channel *crl = sg->createChannel(acor, acol);
    crl->setName(c->getName() + "-latency-1");
    crl->getSrcPort()->setRateOfScenario(s, 1);
    crl->getDstPort()->setRateOfScenario(s, 1);
    crl->setInitialTokens(0);

    // Create channel from actor which models connection latency to actor 
    // which models TDMA synchronization
    Channel *clt = sg->createChannel(acol, at);
    clt->setName(c->getName() + "-tdma-1");
    clt->getSrcPort()->setRateOfScenario(s, 1);
    clt->getDstPort()->setRateOfScenario(s, 1);
    clt->setInitialTokens(0);
    
    // Create channel from actor which models TDMA synchronization to 
    // destination actor of the channel
    Channel *ctd = sg->createChannel(at, c->getDstActor());
    ctd->setName(c->getName() + "-tdma-2");
    ctd->getSrcPort()->setRateOfScenario(s, 1);
    ctd->getDstPort()->setRateOfScenario(s,c->getDstPort()->getRateOfScenario(s));
    ctd->setInitialTokens(0);

    // Create channel from source actor to actor which models channel delay
    Channel *csd = sg->createChannel(c->getSrcActor(), ad);
    csd->setName(c->getName() + "-latency-2");
    csd->getSrcPort()->setRateOfScenario(s,c->getSrcPort()->getRateOfScenario(s));
    csd->getDstPort()->setRateOfScenario(s, 1);
    csd->setInitialTokens(c->getInitialTokens());

    // Create channel actor which models channel delay to destination actor
    Channel *cdd = sg->createChannel(ad, c->getDstActor());
    cdd->setName(c->getName() + "-latency-3");
    cdd->getSrcPort()->setRateOfScenario(s, 1);
    cdd->getDstPort()->setRateOfScenario(s,c->getDstPort()->getRateOfScenario(s));
    cdd->setInitialTokens(0);
}

/**
 * modelBindingInNSoCFlow ()
 * Model all binding decisions in a graph.
 */
void BindingAwareGraph::modelBindingInNSoCFlow()
{
    // Iterate over all scenario graphs in the binding aware graph
    for (ScenarioGraphs::iterator i = 
            bindingAwareGraph->getScenarioGraphs().begin();
            i != bindingAwareGraph->getScenarioGraphs().end(); i++)
    {
        ScenarioGraph *sg = *i;
        
        // Get scenario which corresponds to this graph (this relation is
        // unique since a graph with isolated scenario is used).
        Scenario *s = getScenarioOfScenarioGraph(sg);
        
        // Keep list with original channels in the scenario graph. Only these
        // channels need to be modeled. Channels added by this function should
        // be ignored.
        Channels channels = sg->getChannels();
        
        // Iterate over all actors in the graph
        for (Actors::iterator j = sg->getActors().begin();
                j != sg->getActors().end(); j++)
        {
            map<Actor*, Processor*>::iterator pb;
            Actor *a = *j;
            
            // Get binding of actor a to a processor
            pb = bindingAwareProperties[sg].actorToProcessor.find(a);
            
            // No binding of actor to processor exists?
            if (pb == bindingAwareProperties[sg].actorToProcessor.end())
                throw CException("All actors must be bound to a tile.");
            
            // Model binding of actor a to processor p
            createMappedActorNSoC(a, pb->second, s);
        }
        
        // Iterate over all channels in the graph
        for (Channels::iterator j = channels.begin(); j != channels.end(); j++)
        {
            map<Channel*, Connection*>::iterator cb;
            ChannelBindingConstraints *bc;
            Channel *c = *j;
            
            // Get binding constraints of channel c
            bc = graphBindingConstraints->getConstraintsOfScenario(s)
                                        ->getConstraintsOfChannel(c);
            
            // Get binding of channel c to a connection
            cb = bindingAwareProperties[sg].channelToConnection.find(c);
            
            // No binding of channel to connection exists?
            if (cb == bindingAwareProperties[sg].channelToConnection.end())
            {
                // Model binding of channel c to a tile
                createMappedChannelToTileNSoC(c, bc, s);
            }
            else
            {
                // Model binding of channel c to connection co
                createMappedChannelToConnectionNSoC(c, bc, cb->second, s);
            }
        }
        
    }
}

} // End namespace FSMSADF

