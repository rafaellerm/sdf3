/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   binding_aware_sdfg.h
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   May 14, 2009
 *
 *  Function        :   Binding-aware SDFG
 *
 *  History         :
 *      14-05-09    :   Initial version.
 *
 * $Id: binding_aware_graph.h,v 1.1 2009-12-23 13:37:21 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#ifndef FSMSADF_RESOURCE_ALLOCATION_BINDING_AWARE_GRAPH_BINDING_AWARE_GRAPH_H_INCLUDED
#define FSMSADF_RESOURCE_ALLOCATION_BINDING_AWARE_GRAPH_BINDING_AWARE_GRAPH_H_INCLUDED

#include "../flow/base/type.h"
#include "../platform_binding/graph.h"

namespace FSMSADF
{

/**
 * BindingAwareGraph ()
 * Binding-aware Graph.
 */
class BindingAwareGraph
{
public:
    class BindingAwareProperties
    {
    public:
        // Actor to processor mapping
        map<Actor*, Processor*> actorToProcessor;
        
        // Channel to connection mapping
        map<Channel*, Connection*> channelToConnection;
        
        // Processor to graph allocations mappings
        map<Processor*, StaticOrderSchedule*> processorToStaticOrderSchedule;
        map<Processor*, Time> processorToTimeslice;
        
    public:
        // Actor to processor mapping
        bool actorMappedToProcessor(Actor *a) const;

        // Channel to connection mapping
        bool channelMappedToConnection(Channel *c) const;
    };

public:
    // Constructor
    BindingAwareGraph(PlatformBinding *b, const FlowType flowType);
    
    // Destructor
    ~BindingAwareGraph();

    // Extract actor and channel binding and scheduling properties
    void extractActorMapping(PlatformBinding *b);
    void extractChannelMapping(PlatformBinding *b);
    void extractGraphBindingConstraints(PlatformBinding *b);
    
    // Model binding of mapping of the application to platform in an graph
    void constructBindingAwareGraph(PlatformBinding *b, 
            const FlowType flowType);

    // The actual modeling functions for the NSoC flow
    void createMappedActorNSoC(Actor *a, Processor *p, Scenario *s);   
    void createMappedChannelToTileNSoC(Channel *c, 
            ChannelBindingConstraints *bc, Scenario *s);
    void createMappedChannelToConnectionNSoC(Channel *c, 
            ChannelBindingConstraints *bc, Connection *co, Scenario *s);
    void modelBindingInNSoCFlow();

private:
    // Find scenario related to a scenario graph
    Scenario *getScenarioOfScenarioGraph(const ScenarioGraph *sg) const;

public:
    // Binding-aware properties of each scenario graph
    map<ScenarioGraph*, BindingAwareProperties> bindingAwareProperties;

    // Application graph binding constraints
    GraphBindingConstraints *graphBindingConstraints;

    // Binding-aware graph
    Graph *bindingAwareGraph;
};

} // End namspace FSMSADF

#endif

