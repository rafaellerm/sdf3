/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   edf.cc
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   June 2, 2009
 *
 *  Function        :   Earliest deadline first scheduling
 *
 *  History         :
 *      02-06-09    :   Initial version.
 *
 * $Id: edf.cc,v 1.1 2009-12-23 13:37:23 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "edf.h"

namespace FSMSADF
{

/**
 * schedule()
 * Create earliest-deadline-first schedule for each scenario graph in the
 * platform binding. These schedules are added to the processors inside the
 * platform binding.
 */
void EarliestDeadLineFirstScheduling::schedule(PlatformBinding *b, 
        const FlowType flowType)
{
    BindingAwareGraph *bg;

    // Create a binding-aware graph bg in which the TDMA 
    // synchronization, latency and connection delay are modelled
    bg = new BindingAwareGraph(b, flowType);

    // Construct schedule for each scenario in the binding-aware graph
    for (Scenarios::iterator i = bg->bindingAwareGraph->getScenarios().begin();
            i != bg->bindingAwareGraph->getScenarios().end(); i++)
    {
        Scenario *s = *i;
        
        // Construct schedule for binding-aware graph bg in scenario s
        scheduleScenario(b, bg, s);
    }

    // Cleanup
    delete bg;
}

/**
 * scheduleScenario()
 * Create an earliest-deadline-first schedule for the binding-aware graph bg in 
 * scenario s.
 */
void EarliestDeadLineFirstScheduling::scheduleScenario(PlatformBinding *b,
        BindingAwareGraph *bg, Scenario *s)
{
    // Scenario s belongs to binding-aware graph - find corresponding scenario
    // in the application graph.
    Scenario *sApp = b->getApplicationGraph()->getScenario(s->getName());
    
    // Create precedence graph for scenario s
    PrecedenceGraph *pg = new PrecedenceGraph(bg, s);

    // Scenario graph associated with scenario s
    ScenarioGraph *sg = s->getScenarioGraph();

    // Binding-aware properties
    BindingAwareGraph::BindingAwareProperties &bgProperties 
                                               = bg->bindingAwareProperties[sg];

    // Create empty static-order schedule on each processor
    for (TileBindings::iterator i = b->getTileBindings().begin();
            i != b->getTileBindings().end(); i++)
    {
        TileBinding *tb = *i;
        
        // Iterate over all processors
        for (ProcessorBindings::iterator j = tb->getProcessorBindings().begin();
                j != tb->getProcessorBindings().end(); j++)
        {
            ProcessorBinding *pb = *j;
            StaticOrderSchedule so;
            
            // Assign empty schedule in scenario sto the processor binding
            pb->setStaticOrderSchedule(sApp, so);
            
            // Update the binding-aware graph to use the new, empty schedule
            bg->bindingAwareProperties[sg]
                    .processorToStaticOrderSchedule[pb->getProcessor()] 
                        = &(pb->getStaticOrderSchedule(sApp));
        }
    }

    // Iterate over all nodes in the precedence graph from highest to lowest
    // deadline
    for (PrecedenceGraph::Node *n = pg->getEarliestDeadline();
            n != NULL; n = pg->getEarliestDeadline())
    {
        Actor *a = n->actor;
        
        // Actor mapped to a processor
        if (bg->bindingAwareProperties[sg].actorMappedToProcessor(a))
        {
            Processor *p = bg->bindingAwareProperties[sg].actorToProcessor[a];
            StaticOrderSchedule *so = bg->bindingAwareProperties[sg]
                                            .processorToStaticOrderSchedule[p];
            
            // Add actor to end of static order schedule so
            so->insertActor(so->end(), a);
            
            // First static-order schedule entry on each processor becomes
            // start of periodic schedule
            so->setStartPeriodicSchedule(0);
        }
        
        // Remove node from precedence graph
        pg->removeNode(n);
    }

    // Relabel the actors in the schedule to actors from the application graph
    for (map<Processor*, StaticOrderSchedule*>::iterator
            i = bgProperties.processorToStaticOrderSchedule.begin();
            i != bgProperties.processorToStaticOrderSchedule.end(); i++)
    {
        StaticOrderSchedule *so = i->second;
        
        // Corresponding scenario graph in the application graph
        ScenarioGraph *sgApp = b->getApplicationGraph()->getScenario(
                                              s->getName())->getScenarioGraph();
        
        // Change actor associations in schedule to actors from scenario
        // graph of the application graph
        so->changeActorAssociations(sgApp);
    }
    
    // Cleanup
    delete pg;
}

} // End namespace FSMSADF

