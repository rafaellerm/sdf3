/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   memory.cc
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   May 12, 2009
 *
 *  Function        :   Tile memory.
 *
 *  History         :
 *      12-05-09    :   Initial version.
 *
 * $Id: memory.cc,v 1.1 2009-12-23 13:37:22 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "memory.h"
#include "graph.h"

namespace FSMSADF
{

/**
 * MemoryBinding()
 * Constructor.
 */
MemoryBinding::MemoryBinding(GraphComponent c, Memory *m)
    : 
        GraphComponent(c),
        memory(m),
        memorySizeUsedForOtherGraphs(0)
{
}

/**
 * ~MemoryBinding()
 * Destructor.
 */
MemoryBinding::~MemoryBinding()
{
}

/**
 * clone()
 * Create a cloned copy of this memory binding.
 */
MemoryBinding *MemoryBinding::clone(GraphComponent c)
{
    MemoryBinding *mb = new MemoryBinding(c, getMemory());
    
    // Actors
    mb->setActorBindings(getActorBindings());
    
    // Channels
    mb->setChannelBindings(getChannelBindings());
    
    // Memory size used for other graphs
    mb->setMemorySizeUsedForOtherGraphs(getMemorySizeUsedForOtherGraphs());

    return mb;    
}

/**
 * constructFromXML()
 * This function creates the memory binding as specified by the 
 * connectionNode.
 */
void MemoryBinding::constructFromXML(Scenario *s, 
        const CNodePtr memoryNode)
{
    // Application graph
    ScenarioGraph *sg = s->getScenarioGraph();

    // Iterate over the list of actors bound to this memory
    for (CNode *n = CGetChildNode(memoryNode, "actor");
            n != NULL; n = CNextNode(memoryNode, "actor"))
    {
        // Name specified for the actor?
        if (!CHasAttribute(n, "name"))
            throw CException("Mapped actor has no name identifier.");
    
        // Processor type specified for the actor?
        if (!CHasAttribute(n, "processorType"))
            throw CException("Mapped actor has no processorType identifier.");
    
        // Find actor in scenario graph
        Actor *a = sg->getActor(CGetAttribute(n, "name"));

        // Find processor type in actor
        const Actor::ProcessorType &p = a->getProcessorType(CGetAttribute(n, "processorType"));
        
        // Create binding of actor to memory
        addActorBinding(s, a, p);
    }

    // Iterate over the list of channels bound to this memory
    for (CNode *n = CGetChildNode(memoryNode, "channel");
            n != NULL; n = CNextNode(memoryNode, "channel"))
    {
        // Name specified for the channel?
        if (!CHasAttribute(n, "name"))
            throw CException("Mapped channel has no name identifier.");
    
        // Buffer type specified for the actor?
        if (!CHasAttribute(n, "bufferType"))
            throw CException("Mapped actor has no bufferType identifier.");
    
        // Find channel in scenario graph
        Channel *c = sg->getChannel(CGetAttribute(n, "name"));

        // Buffer location
        BufferSize b;
        BufferSize::Loc bufferLocation = b.asLoc(CGetAttribute(n, "bufferType"));
        
        // Create binding of channel to memory
        addChannelBinding(s, c, bufferLocation);
    }
}

/**
 * constructResourceUsageFromXML()
 * This function sets the resource usgae of the memoru.
 */
void MemoryBinding::constructResourceUsageFromXML(const CNodePtr memoryNode)
{
    // Used memory
    if (!CHasAttribute(memoryNode, "size"))
        throw CException("Memoy resource usage has no size.");
    setMemorySizeUsedForOtherGraphs(CGetAttribute(memoryNode, "size"));
}

/**
 * convertToXML()
 * This function converts the memory binding to an XML object.
 */
void MemoryBinding::convertToXML(Scenario *s, const CNodePtr memoryNode)
{
    map<Scenario*, ActorBindings>::iterator scenActorIter;
    map<Scenario*, ChannelBindings>::iterator scenChannelIter;
    
    // Name
    CAddAttribute(memoryNode, "name", getName());

    // Actor binding exists for this scenario?
    scenActorIter = actorBindings.find(s);
    if (scenActorIter != actorBindings.end())
    {
        // List of all actors bound to this memory
        for (ActorBindings::iterator i = scenActorIter->second.begin();
                i != scenActorIter->second.end(); i++)
        {
            CNode *n = CAddNode(memoryNode, "actor");
            CAddAttribute(n, "name", i->actor->getName());
            CAddAttribute(n, "processorType", i->processorType.type);
        }
    }

    // Channel binding exists for this scenario?
    scenChannelIter = channelBindings.find(s);
    if (scenChannelIter != channelBindings.end())
    {
        // List of all channels bound to this memory
        for (ChannelBindings::iterator i = scenChannelIter->second.begin();
                i != scenChannelIter->second.end(); i++)
        {
            BufferSize b;
            CNode *n = CAddNode(memoryNode, "channel");
            CAddAttribute(n, "name", i->channel->getName());
            CAddAttribute(n, "bufferType", b.asString(i->bufferLocation));
        }
    }
}

/**
 * convertResourceUsageToXML()
 * This function converts the resource usage of this memory binding to an 
 * XML object.
 */
void MemoryBinding::convertResourceUsageToXML(const CNodePtr memoryNode)
{
    // Name
    CAddAttribute(memoryNode, "name", getName());

    // Used memory
    CAddAttribute(memoryNode, "size", getMemorySizeUsedForGraph() 
                                      + getMemorySizeUsedForOtherGraphs());
}

/**
 * getGraphBindingConstraints()
 * Get the graph binding constraints associated with this binding.
 */
GraphBindingConstraints* MemoryBinding::getGraphBindingConstraints() const
{
    return getTileBinding()->getPlatformBinding()->getGraphBindingConstraints();
}

/**
 * addActorBinding()
 * The function binds actor a in scenario s to this memory. The function
 * returns true on success. Otherwise it returns false.
 */
bool MemoryBinding::addActorBinding(Scenario *s, Actor *a, 
        const Actor::ProcessorType &t)
{
    // Binding already exists?
    if (hasActorBinding(s, a))
        return true;
        
    // Sufficient resources available?
    if (hasResourcesActorBinding(s, a, t))
    {
        actorBindings[s].push_back(ActorBinding(a,t));
        return true;
    }
    
    return false;
}

/**
 * removeActorBinding()
 * The function removes the binding of actor a in scenario s to this 
 * memory.
 */
void MemoryBinding::removeActorBinding(Scenario *s, const Actor *a)
{
    for (ActorBindings::iterator i = actorBindings[s].begin();
            i != actorBindings[s].end(); i++)
    {
        if (i->actor == a)
        {
            actorBindings[s].erase(i);
            break;
        }
    }
}

/**
 * hasActorBinding()
 * The function returns true when a binding of actor a in scenario s to this 
 * memory exists. Otherwise it returns false.
 */
bool MemoryBinding::hasActorBinding(Scenario *s, const Actor *a) const
{
    map<Scenario*, ActorBindings>::const_iterator scenIter;
    
    scenIter = actorBindings.find(s);
    if (scenIter == actorBindings.end())
        return false;
        
    for (ActorBindings::const_iterator i = scenIter->second.begin();
            i != scenIter->second.end(); i++)
    {
        if (i->actor == a)
            return true;
    }
    
    return false;    
}

/**
 * hasResourcesActorBinding()
 * The function checks whether there are sufficient resources available
 * to bind actor a to this memory when the actor will be mapped to the
 * specified processor type.
 */
bool MemoryBinding::hasResourcesActorBinding(Scenario *s, 
        const Actor *a, const Actor::ProcessorType &t) const
{
    if (getAvailableMemorySize(s) >= 
            a->getStateSizeOfScenario(s, t.type))
    {
        return true;
    }
    
    return false;
}

/**
 * addChannelBinding()
 * The function binds channel c in scenario s to this memory. The function
 * returns true on success. Otherwise it returns false.
 */
bool MemoryBinding::addChannelBinding(Scenario *s, Channel *c, 
        BufferSize::Loc l)
{
    // Binding already exists?
    if (hasChannelBinding(s, c))
        return true;
        
    // Sufficient resources available?
    if (hasResourcesChannelBinding(s, c, l))
    {
        channelBindings[s].push_back(ChannelBinding(c,l));
        return true;
    }
    
    return false;
}

/**
 * removeChannelBinding()
 * The function removes the binding of channel c in scenario s to this 
 * memory.
 */
void MemoryBinding::removeChannelBinding(Scenario *s, const Channel *c)
{
    for (ChannelBindings::iterator i = channelBindings[s].begin();
            i != channelBindings[s].end(); i++)
    {
        if (i->channel == c)
        {
            channelBindings[s].erase(i);
            break;
        }
    }
}

/**
 * hasChannelBinding()
 * The function returns true when a binding of channel c in scenario s to this 
 * memory exists. Otherwise it returns false.
 */
bool MemoryBinding::hasChannelBinding(Scenario *s, const Channel *c) const
{
    map<Scenario*, ChannelBindings>::const_iterator scenIter;
    
    scenIter = channelBindings.find(s);
    if (scenIter == channelBindings.end())
        return false;
        
    for (ChannelBindings::const_iterator i = scenIter->second.begin();
            i != scenIter->second.end(); i++)
    {
        if (i->channel == c)
            return true;
    }
    
    return false;    
}

/**
 * hasResourcesChannelBinding()
 * The function checks whether there are sufficient resources available
 * to bind channel c to this memory.
 */
bool MemoryBinding::hasResourcesChannelBinding(Scenario *s, 
        const Channel *c, const BufferSize::Loc &l) const
{
    ChannelBindingConstraints *cb;
    cb = getGraphBindingConstraints()->getConstraintsOfScenario(s)
                                     ->getConstraintsOfChannel(c);
                
    if (getAvailableMemorySize(s) > cb->getBufferSize()[l])
    {
        return true;
    }
    
    return false;
}

/**
 * getAvailableMemorySize()
 * The function returns the amount of memory which is available in this memory
 * in scenario s.
 */
Size MemoryBinding::getAvailableMemorySize(Scenario *s) const
{
    return (memory->getSize()
                - getAllocatedMemorySize(s)
                - memorySizeUsedForOtherGraphs);
}

/**
 * getAllocatedMemorySize()
 * The function returns the amount of memory which has been allocated to the
 * application in scenario s.
 */
Size MemoryBinding::getAllocatedMemorySize(Scenario *s) const
{
    map<Scenario*, ChannelBindings>::const_iterator channelScenIter;
    map<Scenario*, ActorBindings>::const_iterator actorScenIter;
    Size szActors = 0;
    Size szChannels = 0;

    // Actors require max state size of all bound actors within scenario s
    actorScenIter = actorBindings.find(s);
    if (actorScenIter != actorBindings.end())
    {
        for (ActorBindings::const_iterator i = actorScenIter->second.begin();
                i != actorScenIter->second.end(); i++)
        {
            Actor *a = i->actor;
            CString t = i->processorType.type;
            szActors = MAX(szActors, a->getStateSizeOfScenario(s, t));
        }
    }
    
    // Channels require sum of requirements per channel
    channelScenIter = channelBindings.find(s);
    if (channelScenIter != channelBindings.end())
    {
        for (ChannelBindings::const_iterator i = channelScenIter->second.begin();
                i != channelScenIter->second.end(); i++)
        {
            ChannelBindingConstraints *cb;
            Channel *c = i->channel;
            BufferSize::Loc l = i->bufferLocation;
            cb = getGraphBindingConstraints()->getConstraintsOfScenario(s)
                                             ->getConstraintsOfChannel(c);
            szChannels += cb->getBufferSize()[l];
        }
    }
    
    return szActors + szChannels;
}

/**
 * getMemorySizeUsedForGraph()
 * The function returns the maximal amount of memory which has been allocated 
 * to the application in any of its scenarios.
 */
Size MemoryBinding::getMemorySizeUsedForGraph() const
{
    // Application graph
    Graph *g = getTileBinding()->getPlatformBinding()->getApplicationGraph();
    Size maxSize = 0;
    
    for (Scenarios::iterator i = g->getScenarios().begin();
            i != g->getScenarios().end(); i++)
    {
        maxSize = MAX(maxSize, getAllocatedMemorySize(*i));
    }
    
    return maxSize;
}

} // End namespace

