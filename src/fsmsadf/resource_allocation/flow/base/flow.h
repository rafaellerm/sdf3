/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   flow.h
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   May 14, 2009
 *
 *  Function        :   Graph mapping to MP-SoC
 *
 *  History         :
 *      29-05-09    :   Initial version.
 *
 * $Id: flow.h,v 1.2 2010-02-08 08:04:42 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#ifndef FSMSADF_RESOURCE_ALLOCATION_FLOW_BASE_FLOW_H_INCLUDED
#define FSMSADF_RESOURCE_ALLOCATION_FLOW_BASE_FLOW_H_INCLUDED

#include "../memory/memory.h"
#include "../tile/binding.h"
#include "type.h"

namespace FSMSADF
{

/**
 * Graph mapping to MP-SoC flow
 */
class SDF3Flow
{
public:
    // State of the mapping flow
    enum FlowState { FlowStart, 
                     FlowComputeStorageDist,
                     FlowSelectStorageDist,
                     FlowEstimateStorageDist,
                     FlowEstimateBandwidthConstraint,
                     FlowBindGraphtoTile,
                     FlowEstimateConnectionDelay,
                     FlowConstructTileSchedules,
                     FlowCompleted,
                     FlowFailed };
    
public:
    // Constructor
    SDF3Flow(FlowType type);
    
    // Destructor
    ~SDF3Flow();

    // Construct
    void constructFromXML(const CNodePtr sdf3Node);

    // Convert
    void convertToXML(const CNodePtr sdf3Node);
    void convertToHTML(const CString &dirname);
    
    // Flow type
    FlowType getFlowType() const { return flowType; };
    
    // Execute design flow
    FlowState run();
    
    // Run flow step-by-step
    bool getStepMode() const { return stepMode; };
    void setStepMode(bool flag) { stepMode = flag; };

    // Tile binding and scheduling algorithm
    MemoryDimAlgo *getMemoryDimAlgo() const { return memoryDimAlgo; };
    void setMemoryDimAlgo(MemoryDimAlgo *a) { memoryDimAlgo = a; };
    
    // Tile binding and scheduling algorithm
    TileBindingAlgo *getTileBindingAlgo() const { return tileBindingAlgo; };
    void setTileBindingAlgo(TileBindingAlgo *a) { tileBindingAlgo = a; };
    
    // Restrict the number of platform bindings generated by the tile binding
    // and scheduling algorithm
    uint getMaxNrBindingsTileBindingAlgo() const 
            { return maxNrBindingsTileBindingAlgo; };
    void setMaxNrBindingsTileBindingAlgo(const uint n) 
            { maxNrBindingsTileBindingAlgo = n; };
    
    // Application graph
    Graph *getApplicationGraph() { return applicationGraph; };
    
    // Platform graph
    PlatformGraph *getPlatformGraph() { return platformGraph; };

    // Platform bindings
    PlatformBindings &getPlatformBindings() { return platformBindings; };
    
private:
    // State of the design flow
    void setNextStateOfFlow(FlowState s) { stateOfFlow = s; };
    FlowState getStateOfFlow() { return stateOfFlow; };

    // Output the SDF3 flow object as XML                   
    void outputAsXML(ostream &out);

private:
    // Steps of the design flow
    void checkInputDesignFlow();
    void computeStorageDist();
    void selectStorageDist();
    void estimateStorageDist();
    void estimateBandwidthConstraint();
    void bindGraphtoTiles();
    void estimateConnectionDelay();
    void constructTileSchedules();

    // User interaction when running flow in step-by-step mode
    void handleUserInteraction();

private:
    // Flow type
    FlowType flowType;
    
    // Run flow step-by-step
    bool stepMode;
    
    // Application graph
    Graph *applicationGraph;
    
    // Platform graph
    PlatformGraph *platformGraph;
    
    // Platform bindings
    PlatformBindings platformBindings;
    
    // Memory dimensioning object
    MemoryDimAlgo *memoryDimAlgo;
    
    // Tile binding and scheduling object
    TileBindingAlgo *tileBindingAlgo;
    
    // State of the design flow
    FlowState stateOfFlow;
    
    // Maximum number of platform bindings created in tile binding algorithm
    uint maxNrBindingsTileBindingAlgo;
};

} // End namespace FSMSADF

#endif
