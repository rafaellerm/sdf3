/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   type.h
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   May 14, 2009
 *
 *  Function        :   List of different flow types supported by SDF3
 *
 *  History         :
 *      29-05-09    :   Initial version.
 *
 * $Id: type.h,v 1.1 2009-12-23 13:37:22 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#ifndef FSMSADF_RESOURCE_ALLOCATION_FLOW_BASE_TYPE_H_INCLUDED
#define FSMSADF_RESOURCE_ALLOCATION_FLOW_BASE_TYPE_H_INCLUDED

namespace FSMSADF
{
    enum FlowType { FlowTypeNSoC };

} // End namespace FSMSADF

#endif

