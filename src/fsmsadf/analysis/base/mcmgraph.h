/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   mcmgraph.h
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   July 22, 2009
 *
 *  Function        :   MCM graph.
 *
 *  History         :
 *      22-07-09    :   Initial version.
 *
 * $Id: mcmgraph.h,v 1.1 2009-12-23 13:37:19 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#ifndef FSMSADF_ANALYSIS_BASE_MCMGRAPH_H_INCLUDED
#define FSMSADF_ANALYSIS_BASE_MCMGRAPH_H_INCLUDED

namespace FSMSADF
{

struct _MCMnode;

typedef struct _MCMedge
{
    CId id;
    struct _MCMnode *src;
    struct _MCMnode *dst;
    double w;
    double d;
} MCMedge;

typedef list<MCMedge*>      MCMedges;

typedef struct _MCMnode
{
    CId id;
    MCMedges in;
    MCMedges out;
} MCMnode;

typedef list<MCMnode*>      MCMnodes;

class MCMgraph
{
public:
    // Constructor
    MCMgraph() {};
    
    // Destructor
    ~MCMgraph()
    {
        for (MCMnodes::iterator i = nodes.begin(); i != nodes.end(); i++)
            delete (*i);

        for (MCMedges::iterator i = edges.begin(); i != edges.end(); i++)
            delete (*i);
    };
    
    // Nodes
    MCMnodes nodes;
    uint nrNodes() { return nodes.size(); };
    MCMnode *getNode(uint id) {
        for (MCMnodes::iterator i = nodes.begin(); i != nodes.end(); i++)
            if ((*i)->id == id)
                return (*i);
        return NULL;
    };
    
    // Edges
    MCMedges edges;
    uint nrEdges() { return edges.size(); };
};

typedef list<MCMgraph*>      MCMgraphs;

} // End namespace FSMSADF

#endif

