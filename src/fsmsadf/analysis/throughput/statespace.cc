/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   statespace.cc
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   August 12, 2009
 *
 *  Function        :   FSM-based SADF throughput analysis algorithm using
 *                      state-space traversal.
 *
 *  History         :
 *      12-08-09    :   Initial version.
 *
 * $Id: statespace.cc,v 1.3 2010-05-07 07:32:16 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "statespace.h"
#include "../base/mcmgraph.h"

//#define _DEBUG_

namespace FSMSADF
{

/**
 * throughputAnalysisGraphUsingStateSpace()
 * Compute the throughput of an FSM-based SDAF graph through a state-space
 * traversal.
 */
Throughput throughputAnalysisGraphUsingStateSpace(Graph *g)
{
    MPExploreStateSpace::StateSpaceExploration sse;
    Graph *gt;
    Throughput thr;
    
    // Create a copy of the graph
    gt = g->clone(GraphComponent(g->getParent(), g->getId(), g->getName()));
    
    // Add dummy actor with self-edge to each scenario graph
    for (ScenarioGraphs::iterator i = gt->getScenarioGraphs().begin();
            i != gt->getScenarioGraphs().end(); i++)
    {
        ScenarioGraph *sg = *i;
        
        // Use first channel in the graph
        Channel *cf = sg->getChannels().front();
        
        // Create dummy actor
        Actor *a = sg->createActor();
        a->setName("sync-actor");
        a->setExecutionTimeOfScenario(gt->getDefaultScenario(), "sync", 0);
        a->setDefaultProcessorType("sync");
        
        // Add self-edge to the dummy actor
        Channel *c = sg->createChannel(a, a);
        c->setName("sync-selfedge");
        c->getSrcPort()->setRateOfScenario(gt->getDefaultScenario(), 1);
        c->getDstPort()->setRateOfScenario(gt->getDefaultScenario(), 1);
        c->setInitialTokens(1);
        
        // Attach dummy actor to the scenario graph
        Channel *ca = sg->createChannel(cf->getSrcActor(), a);
        ca->setName("sync-src");
        ca->getSrcPort()->setRate(cf->getSrcPort()->getRate());
        ca->getDstPort()->setRateOfScenario(gt->getDefaultScenario(), 1);
        ca->setInitialTokens(0);

        Channel *cb = sg->createChannel(a, cf->getDstActor());
        cb->setName("sync-dst");
        cb->getSrcPort()->setRateOfScenario(gt->getDefaultScenario(), 1);
        cb->getDstPort()->setRate(cf->getDstPort()->getRate());
        cb->setInitialTokens(cf->getInitialTokens());
    }
    
    // Compute throughput
    thr = sse.exploreThroughput(gt);
        
    // Cleanup
    delete gt;
    
    return thr;    
}

namespace MPExploreStateSpace
{
    /**
     * ActorCount()
     * Constructor.
     */
    ActorCount::ActorCount(RepetitionVector v) : RepetitionVector(v)
    {
    }

    /**
     * isEmpty()
     * The function returns true when all actor counts are zero. Otherwise it 
     * returns false.
     */
    bool ActorCount::isEmpty()
    {
        for (uint i = 0; i < size(); i++)
        {
            if (at(i)>0)
                return false;
        }
        return true;
    }

    /**
     * decreaseValue()
     * Decrement the actor count of actor a.
     */
    void ActorCount::decreaseValue(Actor *a)
    {
        at(a->getId())--;
    }

    /**
     * getValue()
     * The function returns the actor count of actor a.
     */
    uint ActorCount::getValue(Actor *a)
    {
        return at(a->getId());
    }

    /**
     * sum()
     * The function returns the sum of the actor counts.
     */
    uint ActorCount::sum()
    {
        uint s=0;
        
        for (uint i=0; i < size(); i++)
        {
            s += at(i);
        }
        
        return s;
    }

    /**
     * TokenFIFO()
     * Constructor.
     */
    TokenFIFO::TokenFIFO() 
    {
    }
    
    /**
     * ~TokenFIFO()
     * Destructor.
     */
    TokenFIFO::~TokenFIFO()
    {
    }

    /**
     * copy()
     * The function returns a copy of the FIFO.
     */
    TokenFIFO *TokenFIFO::copy()
    {
        return new TokenFIFO(*this);
    }

    /**
     * equals()
     * The function returns true when two FIFO are identical (i.e. same size and 
     * same content).
     */
    bool TokenFIFO::equals(const TokenFIFO *l) const
    {
        if (tokens.size() != l->tokens.size())
            return false;
        
        list<Token>::const_iterator i = tokens.begin();
        list<Token>::const_iterator j = l->tokens.begin();
        
        while (i != tokens.end())
        {
            // Check equality of production times. Consider production times  
            // equal when difference is less then some MP_EPSILON
            // handle -infinity separately
            MPTime ti = i->productionTime;
            MPTime tj = j->productionTime;
            if(MP_ISMINUSINFINITY(tj))
            {
                if(!MP_ISMINUSINFINITY(ti)) 
                    return false;
            }
            else 
            {
                if (fabs(ti - tj) > MP_EPSILON)
                    return false;
            }
            
            // Next
            i++;
            j++;
        }
        return true;
    }

    /**
     * size()
     * The function returns the number of tokens in the FIFO.
     */
    uint TokenFIFO::size() const
    {
        return tokens.size();
    }

    /**
     * getMax()
     * The function returns the largest production time of the tokens in the 
     * FIFO.
     */
    MPTime TokenFIFO::getMax()
    {
        list<Token>::iterator i = tokens.begin();
        MPTime max = MP_MINUSINFINITY;
        
        while (i != tokens.end())
        {
            max = MAX(max, i->productionTime);
                
            // Next
            i++;
        }
        
        return max;
    }

    /**
     * decreaseAll()
     * The function decreases the production time of all tokens in the FIFO by
     * the specified amount.
     */
    void TokenFIFO::decreaseAll(MPDelay amount)
    {
        list<Token>::iterator i = tokens.begin();
        
        while (i != tokens.end())
        {
            i->productionTime -= amount;
            i++;
        }
    }

    /**
     * print()
     * Print the timestamp in the FIFO.
     */
    void TokenFIFO::print(ostream &out) const
    {
        for (list<Token>::const_iterator i = tokens.begin(); 
                i != tokens.end(); i++)
        {
            out << i->productionTime << " ";
        }
    }

    /**
     * State()
     * Constructor.
     */
    State::State(State *prevState, FSMstate *newFsmState)
    {
        // FSM state
        fsmState = newFsmState;
        
        // Token FIFOs
        if (prevState == NULL)
        {
            // Initialize the FIFOs based on the scenario graph associated with
            // the current FSM state
            createInitialTokenFIFOs(fsmState->getScenario()
                                            ->getScenarioGraph());
        }
        else
        {
            // Convert the timestamps in the FIFOs of the previous state to 
            // timestaps in the FIFOs of the scenario graph associated with
            // the current FSM state
            convertTokenFIFOs(prevState, fsmState->getScenario()
                                                     ->getScenarioGraph());
        }
        
        // Delay
        delay = 0;
        
        // Previous state
        previousState = prevState;
    }
        
    /**
     * ~State()
     * Destructor
     */
    State::~State()
    {
        // Cleanup
        for (uint i = 0; i < fifos.size(); i++)
            delete fifos[i];
    }
            
    /**
     * equals()
     * Compare the states for equality on all their token timestamps and
     * equality of the FSM state.
     */
    bool State::equals(const State *s) const
    {
        // Delay
        if (delay != s->delay)
            return false;

        // State have the same FSM state and number of FIFOs?
        if (fsmState != s->fsmState || fifos.size() != s->fifos.size())
            return false;
        
        // Compare all FIFOs for equality
        for (uint i = 0; i < fifos.size(); i++)
            if (!fifos[i]->equals(s->fifos[i]))
                return false;
        
        return true;
    }

    /**
     * createInitialTokenFIFOs()
     * Initialize the FIFOs based on the scenario graph associated with
     * the current FSM state.
     */
    void State::createInitialTokenFIFOs(ScenarioGraph *sg)
    {
        // Allocate token fifos for all channels in the scenario graph
        fifos.resize(sg->getChannels().size());
        for (Channels::iterator i = sg->getChannels().begin();
                i != sg->getChannels().end(); i++)
        {
            Channel *c = *i;
            
            // Allocate an empty token FIFO for this channel
            fifos[c->getId()] = new TokenFIFO;
            
            // Channel contains initial tokens?
            if (c->getInitialTokens() != 0)
            {
                // Add initial tokens to the FIFO
                fifos[c->getId()]->append(0, c->getInitialTokens());
            }
        }
    }
    
    /**
     * convertTokenFIFOs()
     * Convert the timestamps in the FIFOs of the previous state to 
     * timestaps in the FIFOs of the scenario graph associated with
     * the current FSM state
     */
    void State::convertTokenFIFOs(State *previousState, ScenarioGraph *sg)
    {
        ScenarioGraph *sgPrev = previousState->fsmState->getScenario()
                                             ->getScenarioGraph();
        
        // Allocate token fifos for all channels in the scenario graph
        fifos.resize(sg->getChannels().size());
        for (Channels::iterator i = sg->getChannels().begin();
                i != sg->getChannels().end(); i++)
        {
            Channel *c = *i;
            
            // Channel exists in scenario graph associated with the previous
            // state?
            if (sgPrev->hasChannel(c->getName()))
            {
                Channel *cPrev = sgPrev->getChannel(c->getName());
                
                // Copy the associated FIFO from the previous state to this 
                // state
                fifos[c->getId()] = previousState->fifos[cPrev->getId()]->copy();
            }
            else
            {
                // Allocate an empty token FIFO for this channel
                fifos[c->getId()] = new TokenFIFO;

                // Channel contains initial tokens?
                if (c->getInitialTokens() != 0)
                {
                    // Add initial tokens to the FIFO
                    fifos[c->getId()]->append(MP_MINUSINFINITY, 
                                              c->getInitialTokens());
                }
            }
        }
    }

    /**
     * executeIteration()
     * Execute one iteration of the scenario graph associated with this state.
     */
    void State::executeIteration()
    {
        Scenario *s = fsmState->getScenario();
        ScenarioGraph *sg = s->getScenarioGraph();
      
        #ifdef _DEBUG_
        cerr << "execute iteration for state: " << endl;
        print(cerr);
        cerr << endl;
        #endif

        // Compute number of firings needed of each actor in this scenario
        ActorCount actorCnt = computeRepetitionVector(sg, s);

        // Execute one iteration of the graph
        Actors::iterator i = sg->getActors().begin();
        while (!actorCnt.isEmpty())
        {
            uint iterCnt = 0;
            Actor *a;
            
            // Find an enabled actor
            while (true)
            {
                a = *i;
                
                // Actor enabled?
                if (actorCnt.getValue(a) > 0 && actorEnabled(s, a))
                    break;
                
                // Next actor
                if (++i == sg->getActors().end())
                {
                    i = sg->getActors().begin();
                    iterCnt++;
                    if (iterCnt > 1)
                        throw CException("Graph is deadlocked.");
                }
            }
            
            // Fire the enabled actor
            actorCnt.decreaseValue(a);
            fire(s, a);
        }
    }

    /**
     * normalize()
     * Normalize the production time of the tokens in all FIFOs.
     */
    void State::normalize()
    {
        delay = norm();
        
        for (uint i = 0; i < fifos.size(); i++)
        {
            fifos[i]->decreaseAll(delay);
        }
    }

    /**
     * addReachableState()
     * Add the state to the set of reachable states.
     */
    void State::addReachableState(State *state)
    {
        reachableStates.insert(state);
        
        #ifdef _DEBUG_
        cerr << "reached state: " << endl;
        state->print(cerr);
        cerr << "from state: " << endl;
        print(cerr);
        cerr << "with delay: " << state->delay << endl;
        #endif
    }

    /**
     * print()
     * Print the state.
     */
    void State::print(ostream &out) const
    {
        // FIFOs
        for (uint i = 0; i < fifos.size(); i++)
        {
            out << "fifo[" << i << "]: ";
            fifos[i]->print(out);
            out << endl;   
        }
        
        // FSM state
        out << "fsm state: " << fsmState->getName() << endl;
        
        // Delay
        out << "delay: " << delay << endl;
    }

    /**
     * norm()
     * The function returns the norm of the token production times.
     */
    MPTime State::norm()
    {
        MPTime max = MP_MINUSINFINITY;
        
        for (uint i = 0; i < fifos.size(); i++)
        {
            max = MAX(max, fifos[i]->getMax());
        }
        
        return max;
    }

    /**
     * fire()
     * Fire actor a in scenario s.
     */
    void State::fire(Scenario *s, Actor *a)
    {
        MPTime time = MP_MINUSINFINITY, ftime = MP_MINUSINFINITY, fctime;
        
        // Consume input tokens
        for (Ports::iterator i = a->getPorts().begin(); 
                i != a->getPorts().end(); i++)
        {
            Port *p = *i;
            
            // Port is an input port?
            if (p->getType() == Port::In)
            {
                // Consume tokens from input
                time = consume(s, p);
                
                // Update start time of actor firing
                if (i != a->getPorts().begin()) 
                    ftime = MAX(time, ftime);
                else 
                    ftime = time;
            }
        }

        #ifdef _DEBUG_
        cerr << "fire actor " << a->getName() << " in scenario ";
        cerr << s->getName() << " starting at " << time << endl;
        #endif
                
        // Advance time with execution time of actor
        time = ftime + a->getExecutionTimeOfScenario(s, 
                                                  a->getDefaultProcessorType());

        #ifdef _DEBUG_
        cerr << "fire actor " << a->getName() << " in scenario ";
        cerr << s->getName() << " ending at " << time << endl;
        #endif
        
        // Produce output tokens
        for (Ports::iterator i = a->getPorts().begin(); 
                i != a->getPorts().end(); i++)
        {
            Port *p = *i;
            
            // Port is an output port?
            if (p->getType() == Port::Out)
            {
                // Guarantee monotonic time stamps on channel
                //fctime = MAX(time, fifos[p->getChannel()->getId()]->getMax());
                fctime = time;
                
                // Produce tokens on output
                produce(s, p, fctime);
            }
        }
    }

    /**
     * actorEnabled()
     * The function returns true when the actor is enabled (i.e. ready to fire).
     * Otherwise the function returns false.
     */
    bool State::actorEnabled(Scenario *s, Actor* a)
    {
        // Check all ports
        for (Ports::iterator i = a->getPorts().begin();
                i != a->getPorts().end(); i++)
        {
            Port *p = *i;
            
            // Is this port an input port?
            if (p->getType() == Port::In)
            {
                // Not enough tokens available?
                if (fifos[p->getChannel()->getId()]->size() 
                        < p->getRateOfScenario(s))
                {
                    return false;
                }
            }
        }
        
        return true;
    }

    /**
     * exploreThroughput()
     * Compute the throughput of the FSM-based SADF graph using a state-space 
     * traversal.
     */
    Throughput StateSpaceExploration::exploreThroughput(Graph *g)
    {
        State *curState, *newState, *initialState, *dummyState;
        States *visitedStates = new States;
        States *stateQueue = new States;
        Throughput thr;
        
        // Create a dummy state based on the initial FSM state
        dummyState = new State(NULL, g->getFSM()->getInitialState());
        
        // Create an initial state based on the initial FSM state
        initialState = new State(dummyState, g->getFSM()->getInitialState());

        // Add the initial state to the set of visited states
        visitedStates->insert(initialState);

        #ifdef _DEBUG_
        cerr << "insert state: "<< endl;
        initialState->print(cerr);
        cerr << endl;
        #endif
        
        // Create a state based on the initial state
        curState = new State(initialState, g->getFSM()->getInitialState());
        
        // Add state to the state queue
        stateQueue->insert(curState);

        // Explore state-space
        while (!stateQueue->empty())
        {
            // Take the first state from the queue
            curState = *stateQueue->begin();
            stateQueue->erase(stateQueue->begin());
            
            // Execute one iteration of the scenario graph
            curState->executeIteration();
            
            // Normalize state
            curState->normalize();

            #ifdef _DEBUG_
            cerr << endl;
            cerr << "reached state: "<< endl;
            curState->print(cerr);
            cerr << endl;
            #endif
            
            // Reached this state before?
            if (visitedStates->includes(curState))
            {
                // Get recurrent state from the set of visisted states
                State *s = *visitedStates->find(curState);
                
                #ifdef _DEBUG_
                cerr << "recurrent state:" << endl;
                s->print(cerr);
                cerr << endl;
                #endif
                
                // Previous state can go to recurrent state
                curState->previousState->addReachableState(s);
                
                // Cleanup
                delete curState;
            }
            else
            {
                // Add state to the set of reachable states of the previous 
                // state
                curState->previousState->addReachableState(curState);
                
                // Add state to the set of visited states
                visitedStates->insert(curState);
                
                #ifdef _DEBUG_
                cerr << endl;
                cerr << "insert state: "<< endl;
                curState->print(cerr);
                cerr << endl;
                #endif
                
                // Create new reachable state from this state
                for (FSMtransitions::iterator 
                        i = curState->fsmState->getTransitions().begin();
                        i != curState->fsmState->getTransitions().end(); i++)
                {
                    FSMtransition *t = *i;
                    
                    // Create a new state based on this transition
                    newState = new State(curState, t->getDstState());
                    
                    // Add new state to the state queue
                    stateQueue->insert(newState);
                }
            }
        }
        
        #ifdef _DEBUG_
        ofstream dot("statespace.dot");
        printStateSpace(visitedStates, dot);
        #endif
        
        // Compute throughput
        thr = computeThroughput(visitedStates, initialState);
        
        // Cleanup
        for (States::iterator i = visitedStates->begin();
                i != visitedStates->end();)
        {
            State *s = *i;
            visitedStates->erase(i++);
            delete s;
        }
        delete visitedStates;
        delete stateQueue;
        delete dummyState;
        
        return thr;
    }
    
    /**
     * computeThroughput()
     * Perform a DFS on the state-space to discover the MCM of the space. The
     * inverse of the MCM is the throughput of the graph.
     */
    Throughput StateSpaceExploration::computeThroughput(States *visitedStates, 
            State *initialState)
    {
        #ifdef _COMPUTE_MCM_USING_DFS                
        map<State*,State*> parents; 
        States color;
        #endif
        double mcm;

        #ifdef _DEBUG_
        cerr << endl;
        cerr << "Throughput computation:" << endl;
        #endif

        #ifdef _COMPUTE_MCM_USING_DFS
        // Compute mcm using DFS
        parents[initialState] = NULL;
        mcm = dfsStateSpace(visitedStates, initialState, parents, color);
        #else
        // Compute mcm using Karp
        mcm = computeMCM(visitedStates, initialState);
        #endif
        
        // Throughput is zero?
        if (mcm == 0)
            return 0;
        
        return Throughput(1.0/mcm);
    }

    /**
     * dfsStateSpace()
     * Perform a DFS on the state-space to discover the MCM of the space.
     */
    double StateSpaceExploration::dfsStateSpace(States *states, State *state, 
            map<State*,State*> &parents, States &color)
    {
        double mcm = 0, cm;

        // Mark state as visited
        color.insert(state);

        // Continue with all reachable states
        for (set<State*>::iterator i = state->reachableStates.begin();
                i != state->reachableStates.end(); i++)
        {
            // Reached this state before?
            if (color.includes(*i))
            {
                State *s = *i;
                State *p = state;
                double delay = 0;
                double cnt = 0;

                // Backtrack the cycle
                do
                {
                    // Count delay of this edge on the cycle
                    delay = delay + p->delay;
                    cnt = cnt + 1;
                    
                    // Reached end/start of cycle?
                    if (p->equals(*i))
                        break;
                    
                    // Previous edge on the cycle
                    s = p;
                    p = parents[s];
                } while (true);
                
                #ifdef _DEBUG_
                cerr << "found cycle with cm: " << delay << "/" << cnt << endl;
                #endif
                
                // Compute cycle mean
                cm = delay/cnt;
                
                // Update MCM
                mcm = MAX(mcm, cm);
            }
            else
            {
                // Set state as the parent of the state i
                parents[*i] = state;
                
                // Continue DFS
                cm = dfsStateSpace(states, *i, parents, color);
                mcm = MAX(mcm, cm);
            }
        }

        // Mark state as unvisited
        color.erase(state);
        
        return mcm;
    }

    /**
     * computeMCM()
     * The function returns the MCM of the statespace.
     */
    double StateSpaceExploration::computeMCM(States *visitedStates, 
            State *initialState)
    {
        int k, n;
        double **d;
        double l, ld;
        MCMnode *u;
        map<State*,CId> stateIds;
        CId id = 0;
        
        // Create a new MCM graph
        MCMgraph *mcmGraph = new MCMgraph;
        
        // Create the nodes of the MCM graph
        for (States::iterator i = visitedStates->begin();
                i != visitedStates->end(); i++)
        {
            State *s = *i;
            
            // Create an MCM node for this state
            MCMnode *n = new MCMnode;
            n->id = id;

            // Store id of this state
            stateIds[s] = id;
            id++;
            
            // Add the node to the MCM graph
            mcmGraph->nodes.push_back(n);
        }
        
        // Add edges to the MCM graph
        for (States::iterator i = visitedStates->begin();
                i != visitedStates->end(); i++)
        {
            State *s = *i;
            
            // Iterate over the reachable states
            for ( set<State*>::iterator j = s->reachableStates.begin();
                    j != s->reachableStates.end(); j++)
            {
                // Create an edge in the MCM graph for this state transition
                MCMedge *e = new MCMedge;
                e->id = mcmGraph->edges.size();
                e->src = mcmGraph->getNode(stateIds[s]);
                e->dst = mcmGraph->getNode(stateIds[*j]);
                e->w = s->delay;
                e->d = 1;

                // Add the edge to the MCM graph and the src and dst node
                mcmGraph->edges.push_back(e);
                mcmGraph->getNode(stateIds[s])->out.push_back(e);
                mcmGraph->getNode(stateIds[*j])->in.push_back(e);
            }
        }
        
        // Allocate memory
        n = mcmGraph->nrNodes();
        d = new double* [n+1];
        for (int i = 0; i < n+1; i++)
            d[i] = new double [n];

        // Initialize
        for (k = 1; k < n+1; k++)
            for (int u = 0; u < n; u++)
                d[k][u] = -INT_MAX;
        for (int u = 0; u < n; u++)
            d[0][u] = 0;
        
        // Compute the distances
        for (k = 1; k < n+1; k++)
        {
            for (MCMnodes::iterator iter = mcmGraph->nodes.begin();
                    iter != mcmGraph->nodes.end(); iter++)
            {
                MCMnode *v = *iter;
                
                for (MCMedges::iterator e = v->in.begin();
                    e != v->in.end(); e++)
                {
                    MCMnode *u = (*e)->src;
                    
                    d[k][v->id] = MAX(d[k][v->id], d[k-1][u->id] + (*e)->w);
                }
            }
        }
        
        // Compute lamda using Karp's theorem
        l = -INT_MAX;
        for (MCMnodes::iterator iter = mcmGraph->nodes.begin();
                iter != mcmGraph->nodes.end(); iter++)
        {
            u = *iter;
            ld = INT_MAX;
            for (k = 0; k < n; k++)
            {
                ld = MIN(ld, (double)(d[n][u->id]-d[k][u->id]) / (double)(n-k));
            }
            l = MAX(l, ld);
        }

        // Cleanup
        delete mcmGraph;
        for (int i = 0; i < n+1; i++)
            delete [] d[i];
        delete [] d;

        return l;
    }
    
    /**
     * printStateSpace()
     * The function outputs the state-space in DOT format.
     */
    void StateSpaceExploration::printStateSpace(States *states, ostream &out)
    {
        map<State*,uint> stateId;

        out << "digraph statespace {" << endl;
        out << "    size=\"7,10\";" << endl;
        
        for (States::iterator i = states->begin(); i != states->end(); i++)
        {
            State *s = *i;
            uint sId;
            
            // No id assigned to state s?
            if (stateId.find(s) == stateId.end())
                stateId[s] = stateId.size();
            
            // Get id of state s
            sId = stateId[s];
            
            // Output state s as a new node
            out << "    " << sId << " [shape=box label=\"";
 
            // FIFOs
            for (uint j = 0; j < s->fifos.size(); j++)
            {
                out << "fifo[" << j << "]: ";
                for (list<Token>::const_iterator k = s->fifos[j]->tokens.begin(); 
                        k != s->fifos[j]->tokens.end(); k++)
                {
                    out << k->productionTime << " ";
                }
                out << "\\n";   
            }
            
            // FSM state
            out << "fsm state: " << s->fsmState->getName() << "\\n";
            
            // Delay
            out << "delay: " << s->delay << "\\n";

            // Close node
            out << "\"];" << endl;
            
            // Output edges to all reachable states
            for (set<State*>::iterator j = s->reachableStates.begin();
                    j != s->reachableStates.end(); j++)
            {
                State *t = *j;
                uint tId;
                
                // No id assigned to state t?
                if (stateId.find(t) == stateId.end())
                    stateId[t] = stateId.size();
            
                // Get id of state t
                tId = stateId[t];

                // Output edge from state s to state t
                out << "    " << sId << " -> " << tId << " [label=";
                out << t->delay << "];" << endl;
            }
        }
        
        out << "}" << endl;
    }
    
} // End namespace MPExploreStateSpace

} // End namespace FSMSADF

