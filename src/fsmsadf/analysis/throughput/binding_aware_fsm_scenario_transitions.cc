/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   fsm_scenario_transtions.cc
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   May 4, 2009
 *
 *  Function        :   FSM-based SADF throughput analysis algorithm using
 *                      scenario graph and considering scenario transitions.
 *
 *  History         :
 *      04-05-09    :   Initial version.
 *
 * $Id: binding_aware_fsm_scenario_transitions.cc,v 1.1 2009-12-23 13:37:20 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "fsm_ref_schedule.h"
#include "../maxplus/mpexplore_binding_aware.h"
#include "fsm_scenario_transitions.h"

using namespace FSMSADF::MPExploreBindingAware;

namespace FSMSADF
{

/**
 * Throughput analysis considering scenario transitions
 * Compute the throughput of an FSM-based SDAF graph considering the FSM the 
 * schedules of each source and destination scenario in the FSM.
 *
 * Algorithm:
 * 1. Determine for each scenario s the eigenvector v_s and period T_s.
 * 2. The throughput is equal to the MCM of the FSM where the weight of an edge
 *    which represents a scenario transition from scenario s to scenario t is
 *    equal to: d_st = exploreDelayForSchedule(v_s, v_t, T_t).
 */
Throughput throughputAnalysisWithScenarioTransitions(BindingAwareGraph *g)
{
    map<Scenario*, MaxPlus::Vector*> eigenvector;
    map<Scenario*, MPTime> period;
    map<FSMtransition*, MPTime> delay;
    map<Scenario*, Exploration*> mpe;
    Throughput thr;
    
    // The set of scenarios...
    Scenarios scenarios = g->bindingAwareGraph->getScenarios();

    // Create Max-Plus exploration object for each scenario
    for (Scenarios::const_iterator i = scenarios.begin();
            i != scenarios.end(); i++)
    {
        Scenario *s = *i;
        mpe[s] = new Exploration;
        mpe[s]->G = new SGraph(s->getScenarioGraph(), s);
    }

    // Compute eigenvector and period for each scenario
    for (Scenarios::const_iterator i = scenarios.begin();
            i != scenarios.end(); i++)
    {
        Scenario *s = *i;
        MaxPlus::Vector *v_s = NULL;
        MPTime T_s;

        // Compute eigenvector and period of this scenario
        mpe[s]->exploreEigen(&v_s, &T_s);
        
        // Store results
        eigenvector[s] = v_s;
        period[s] = T_s;
    }
    
    // Compute delay for each scenario transition
    for (FSMstates::iterator i = g->bindingAwareGraph->getFSM()->getStates().begin();
            i != g->bindingAwareGraph->getFSM()->getStates().end(); i++)
    {
        Scenario *s = (*i)->getScenario();

        // Iterate over all transitions which are leaving this state
        for (FSMtransitions::iterator j = (*i)->getTransitions().begin();
                j != (*i)->getTransitions().end(); j++)
        {
            Scenario *t = (*j)->getDstState()->getScenario();
            MaxPlus::Vector *v_s = NULL;
            
            // Convert eigen vector of scenario s to eigen vector of scenario t
            v_s = mpe[t]->convertEigenvector(s->getScenarioGraph(), 
                                             eigenvector[s]);

            // Compute delay of this scenario transition
            delay[*j] = mpe[t]->exploreDelayForSchedule(v_s, eigenvector[t], 
                                                        period[t]);

            // Cleanup
            delete v_s;
        }
    }
    
    // Compute MCM of the FSM using delay and period computed with reference
    // schedule
    thr = computeMCMfsm(g->bindingAwareGraph->getFSM(), delay, period);
    thr = CFraction(1) / thr;
    
    // Cleanup
    for (map<Scenario*, MaxPlus::Vector*>::iterator i = eigenvector.begin();
            i != eigenvector.end(); i++)
    {
        delete i->second;
    }
    for (map<Scenario*, Exploration*>::iterator i = mpe.begin();
            i != mpe.end(); i++)
    {
        delete i->second;
    }

    return thr;
}

}  // End namespace FSMSADF

