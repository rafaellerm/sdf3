/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   statespace.h
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   August 12, 2009
 *
 *  Function        :   FSM-based SADF throughput analysis algorithm using
 *                      state-space traversal.
 *
 *  History         :
 *      12-08-09    :   Initial version.
 *
 * $Id: statespace.h,v 1.2 2010-05-07 07:32:16 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#ifndef FSMSADF_ANALYSIS_THROUGHPUT_STATESPACE_H_INCLUDED
#define FSMSADF_ANALYSIS_THROUGHPUT_STATESPACE_H_INCLUDED

#include "../../base/graph.h"
#include "../base/repetition_vector.h"

namespace FSMSADF
{

/**
 * throughputAnalysisGraphUsingStateSpace()
 * Compute the throughput of an FSM-based SDAF graph through a state-space
 * traversal.
 */
Throughput throughputAnalysisGraphUsingStateSpace(Graph *g);

/**
 * namespace MPExploreStateSpace
 * This namespace contains the implementation of the throughput analysis
 * function.
 */
namespace MPExploreStateSpace
{
    /**
     * class ActorCount
     * Associates with every actor a number, representing a number of
     * firings (e.g., a repetition vector).
     */
    class ActorCount : public RepetitionVector
    {
    public:
        // Constructor
        ActorCount(RepetitionVector v);
        
        // All counts are zero
        bool isEmpty();
        
        // Get actor count of actor a
        uint getValue(Actor *a);
        
        // Decrement actor count of actor a
        void decreaseValue(Actor *a);
        
        // Get sum of the actor counts
        uint sum();
    };

    /**
     * class Token
     * Token represents a MaxPlus timestamped token in a graph.
     */
    class Token
    {
    public:
        // Constructor
        Token() { productionTime = 0.0; };
        Token(MPTime t) { productionTime = t; };

    public:
        MPTime productionTime;
    };

    /**
     * class TokenFIFO
     * A token fifo
     */
    class TokenFIFO
    {
    public:
        // Constructor
        TokenFIFO();
        
        // Destructor
        ~TokenFIFO();
        
        // Read N timestamped tokens from the FIFO, return the max of all read
        // timestamps. To save time it is not checked whether the token contains
        // N tokens, only use this method if you are sure there are.
        inline MPTime removeFirstN(uint n)
        {
            MPTime time = MP_MINUSINFINITY;
            
            while (n > 0)
            {
                time = max(tokens.front().productionTime, time);
                tokens.pop_front();
                n--;
            }

            return time;
        }

        // Append n tokens to the FIFO, all timestamped with t. To save time it
        // is not checked whether there is space for the tokens, only use this 
        // method if you are sure there is.
        inline void append(MPTime t, uint n = 1)
        {
            while (n > 0)
            {
                tokens.push_back(Token(t));
                n--;
            }
        }

        // Create a copy of this FIFO
        TokenFIFO* copy();

        // Compare FIFOs for equality
        bool equals(const TokenFIFO *l) const;

        // Get the number of tokens stored in the FIFO
        uint size() const;

        // Get the largest production time of the tokens in the FIFO
        MPTime getMax();
        
        // Decrease the production time of all tokens in the FIFO
        void decreaseAll(MPDelay amount);
        
        // Print the timestamp in the FIFO
        void print(ostream &out) const;
        
    public:
        // The actual FIFO
        list<Token> tokens;
    };

    /**
     * class State
     * State represents a distribution of timestamped tokens over the channels 
     * of the graph and a state in the FSM of the graph.
     */
    class State
    {
    public:
        // Constructor
        State(State *previousState, FSMstate *newFsmState);
        
        // Destructor
        ~State();
    
        // Compare the states for equality on all their token timestamps and
        // equality of the FSM state.
        bool equals(const State *s) const;

        // Create initial FIFOs based on the scenario graph
        void createInitialTokenFIFOs(ScenarioGraph *sg);

        // Convert the FIFOs of the previous state to FIFOs for the scenario
        // graph
        void convertTokenFIFOs(State *previousState, ScenarioGraph *sg);

        // Execute one iteration of the scenario graph associated with this
        // state
        void executeIteration();

        // Normalize the production time of the tokens in all FIFOs. This sets
        // the delay of the state
        void normalize();

        // Add the state to the set of reachable states
        void addReachableState(State *state);

        // Print the state
        void print(ostream &out) const;

    private:
        // Get the norm of the production times of the tokens in the FIFOs
        MPTime norm();

        // Fire actor a in scenario s
        inline void fire(Scenario *s, Actor *a);

        // Consume tokens from port p in scenario s
        inline MPTime consume(Scenario *s, Port *p)
        {
            Channel *c = p->getChannel();
            return fifos[c->getId()]->removeFirstN(p->getRateOfScenario(s));
        }
        
        // Produce tokens on port p in scenario s
        inline void produce(Scenario *s, Port *p, MPTime t)
        {
            Channel *c = p->getChannel();
            fifos[c->getId()]->append(t, p->getRateOfScenario(s));
        }
        
        // Check whether actor a is enabled
        bool actorEnabled(Scenario *s, Actor *a);

    public:
        // Token FIFOs
        vector<TokenFIFO*> fifos;
    
        // FSM state
        FSMstate *fsmState;
        
        // Delay to execute one iteration
        MPDelay delay;
        
        // Reachable states
        set<State*> reachableStates;
        
        // Previous state
        State *previousState;
    };

    /**
     * class States
     * List of states.
     */
    class States : public list<State*>
    {
    public:
        bool includes(State *x, State **y)
        {
            States::const_iterator i = find(x);
            if (i != end())
            {
                (*y) = (*i);
                return true;
            }
            return false;
        }
        bool includes(State *s)
        {
            return find(s) != end();
        }

        // Find state s on the list
        iterator find(State *s)
        {
            for (iterator i = begin(); i != end(); i++)
                if ((*i)->equals(s))
                    return i;
            return end();
        }

        // Insert state s when not in list
        void insert(State *s)
        {
            if (!includes(s))
                push_back(s);
        }
        
        // Erase state s from the list
        void erase(State *s)
        {
            erase(find(s));
        }
        
        // Erase iterator i from the list
        void erase(iterator i)
        {
            list<State*>::erase(i);
        }
    };

    /**
     * class StateSpaceExploration
     * The state-space analysis algorithms.
     */
    class StateSpaceExploration
    {
    public:
        // Compute the throughput of the FSM-based SADF graph using a 
        // state-space traversal
        Throughput exploreThroughput(Graph *g);
    
    private:
        // Compute the throughput of the FSM-based SADF graph by computing the
        // MCM of the state-space
        Throughput computeThroughput(States *visitedStates, 
                State *initialState);

        // Perform a DFS on the state-space to discover the MCM of the space
        double dfsStateSpace(States *states, State *state, 
                map<State*,State*> &parents, States &color);

        // Compute MCM using Karps theorem
        double computeMCM(States *visitedStates, State *initialState);

        // Output the state-space in DOT format
        void printStateSpace(States *states, ostream &out);
    };
    
} // End namespace MPExploreStateSpace

} // End namespace FSMSADF

#endif

