/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   fsm_ref_schedule.cc
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   May 4, 2009
 *
 *  Function        :   FSM-based SADF throughput analysis algorithm using
 *                      scenario graph with reference schedule.
 *
 *  History         :
 *      04-05-09    :   Initial version.
 *
 * $Id: fsm_ref_schedule.cc,v 1.1 2009-12-23 13:37:20 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "scenariograph.h"
#include "fsm_ref_schedule.h"
#include "../base/mcmgraph.h"
#include "../maxplus/mpexplore.h"

using namespace FSMSADF::MPExplore;

namespace FSMSADF
{

Throughput computeMCMfsm(FSM *fsm, map<Scenario*, MPTime> &delay, 
        map<Scenario*, MPTime> &period)
{
    int k, n;
    double **d;
    double l, ld;
    MCMnode *u;

    // Create a new MCM graph
    MCMgraph *mcmGraph = new MCMgraph;
    
    // Create the nodes of the MCM graph
    for (FSMstates::iterator i = fsm->getStates().begin();
            i != fsm->getStates().end(); i++)
    {
        FSMstate *s = *i;
        
        // Create an MCM node for this state
        MCMnode *n = new MCMnode;
        n->id = s->getId();
        
        // Add the node to the MCM graph
        mcmGraph->nodes.push_back(n);
    }
    
    // Add edges to the MCM graph
    for (FSMstates::iterator i = fsm->getStates().begin();
            i != fsm->getStates().end(); i++)
    {
        FSMstate *s = *i;
        
        // Iterate over the transitions of this state
        for (FSMtransitions::iterator j = s->getTransitions().begin();
                j != s->getTransitions().end(); j++)
        {
            // Create an edge in the MCM graph for this transition
            MCMedge *e = new MCMedge;
            e->id = mcmGraph->edges.size();
            e->src = mcmGraph->getNode((*j)->getSrcState()->getId());
            e->dst = mcmGraph->getNode((*j)->getDstState()->getId());
            e->w = delay[s->getScenario()] + period[s->getScenario()];
            e->d = 1;
            
            // Add the edge to the MCM graph and the src and dst node
            mcmGraph->edges.push_back(e);
            mcmGraph->getNode((*j)->getSrcState()->getId())->out.push_back(e);
            mcmGraph->getNode((*j)->getDstState()->getId())->in.push_back(e);
        }
    }
    
    // Allocate memory
    n = mcmGraph->nrNodes();
    d = new double* [n+1];
    for (int i = 0; i < n+1; i++)
        d[i] = new double [n];

    // Initialize
    for (k = 1; k < n+1; k++)
        for (int u = 0; u < n; u++)
            d[k][u] = -INT_MAX;
    for (int u = 0; u < n; u++)
        d[0][u] = 0;
    
    // Compute the distances
    for (k = 1; k < n+1; k++)
    {
        for (MCMnodes::iterator iter = mcmGraph->nodes.begin();
                iter != mcmGraph->nodes.end(); iter++)
        {
            MCMnode *v = *iter;
            
            for (MCMedges::iterator e = v->in.begin();
                e != v->in.end(); e++)
            {
                MCMnode *u = (*e)->src;
                
                d[k][v->id] = MAX(d[k][v->id], d[k-1][u->id] + (*e)->w);
            }
        }
    }
    
    // Compute lamda using Karp's theorem
    l = -INT_MAX;
    for (MCMnodes::iterator iter = mcmGraph->nodes.begin();
            iter != mcmGraph->nodes.end(); iter++)
    {
        u = *iter;
        ld = INT_MAX;
        for (k = 0; k < n; k++)
        {
            ld = MIN(ld, (double)(d[n][u->id]-d[k][u->id]) / (double)(n-k));
        }
        l = MAX(l, ld);
    }

    // Cleanup
    delete mcmGraph;
    for (int i = 0; i < n+1; i++)
        delete [] d[i];
    delete [] d;
    
    return l;
}

/**
 * Throughput analysis using reference schedule
 * Compute the throughput of an FSM-based SDAF graph considering the FSM and a
 * reference schedule.
 *
 * Algorithm:
 * 1. Determine for each scenario s the eigenvector v_s and period T_s.
 * 2. Compute a suitable 'reference schedule'. This can be done by for example,
 *    taking the max over all eigenvectors. An alternative could be to use the
 *    ALAP eigenvector. Currently, the first option is used.
 * 3. Compute the delay for each scenario s using the reference schedule and the
 *    period of scenario s.
 * 4. The throughput is equal to the MCM of the FSM where the weight of a
 *    node which corresponds to scenario s is equal to (d_s + T_s).
 */
Throughput throughputAnalysisUsingRefSchedule(Graph *g)
{
    map<Scenario*, MaxPlus::Vector*> eigenvector;
    map<Scenario*, MPTime> period, delay;
    map<CString, MaxPlus::Vector> v_ref;
    map<Scenario*, Exploration*> mpe;
    Throughput thr;
    
    // The set of scenarios...
    Scenarios scenarios = g->getScenarios();
    
    // Create Max-Plus exploration object for each scenario
    for (Scenarios::const_iterator i = scenarios.begin();
            i != scenarios.end(); i++)
    {
        Scenario *s = *i;
        mpe[s] = new Exploration;
        mpe[s]->G = new SGraph(s->getScenarioGraph(), s);
    }
        
    // Compute eigenvector and period for each scenario
    for (Scenarios::const_iterator i = scenarios.begin();
            i != scenarios.end(); i++)
    {
        Scenario *s = *i;
        MaxPlus::Vector *v_s = NULL;
        MPTime T_s;

        // Compute eigenvector and period of this scenario
        mpe[s]->exploreEigen(&v_s, &T_s);
        
        // Store results
        eigenvector[s] = v_s;
        period[s] = T_s;
    }
    
    // Determine reference schedule
    v_ref = computeReferenceSchedule(eigenvector);
    
    // Compute delay for each scenario
    for (Scenarios::const_iterator i = scenarios.begin();
            i != scenarios.end(); i++)
    {
        Scenario *s = *i;
        MaxPlus::Vector *v_s;
        
        // Compute reference schedule for scenario s
        v_s = getReferenceScheduleForScenario(s, v_ref);
        
        // Compute delay of this scenario
        delay[s] = mpe[s]->exploreDelayForSchedule(v_s, v_s, period[s]);
        
        // Cleanup
        delete v_s;
    }
    
    // Compute MCM of the FSM using delay and period computed with reference
    // schedule
    thr = computeMCMfsm(g->getFSM(), delay, period);
    thr = CFraction(1) / thr;
    
    // Cleanup
    for (map<Scenario*, MaxPlus::Vector*>::iterator i = eigenvector.begin();
            i != eigenvector.end(); i++)
    {
        delete i->second;
    }
    for (map<Scenario*, Exploration*>::iterator i = mpe.begin();
            i != mpe.end(); i++)
    {
        delete i->second->G;
        delete i->second;
    }

    return thr;
}

}  // End namespace FSMSADF

