/*
 *  Eindhoven University of Technology
 *  Eindhoven, The Netherlands
 *
 *  Name            :   sadf2poosl_process.cc
 *
 *  Author          :   Bart Theelen (B.D.Theelen@tue.nl)
 *
 *  Date            :   13 September 2006
 *
 *  Function        :   Output SADF graph in POOSL format (process class(es))
 *
 *  History         :
 *      13-09-06    :   Initial version.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "sadf2poosl_process.h"

void SADF2POOSL_Kernel(SADF_Process* Kernel, ostream &out) {

	bool Timed = false;

	for (CId i = 0; !Timed && i != Kernel->getNumberOfScenarios(); i++)
		for (CId j = 0; !Timed && j != Kernel->getNumberOfProfiles(i); j++)
			if (Kernel->getProfile(i, j)->getExecutionTime() != 0)
				Timed = true;

	out << "process class " << Kernel->getName() << "(Monitor: Boolean, MonitorID: Integer, Trace: Boolean, Name: String)" << endl;
	out << "instance variables" << endl;

	if (Timed)
		out << "ExecutionTimes: Array, ";

	out << "Status: ProcessMonitor" << endl << endl;
	out << "initial method call" << endl;
	out << "Initialise()()" << endl << endl;
	out << "instance methods" << endl;

	out << "Initialise()()";

	if (Timed) {

		out << " |Distribution: GeneralDiscrete|" << endl << endl;
		out << "ExecutionTimes := new(Array) size(" << Kernel->getNumberOfScenarios() << ");" << endl;

		for(CId i = 0; i != Kernel->getNumberOfScenarios(); i++) {

			out << "Distribution := new(GeneralDiscrete)" << endl;

			for (CId j = 0; j != Kernel->getNumberOfProfiles(i); j++) {
	
				out << "   withParameters(";
			
				if (Kernel->getProfile(i, j)->getExecutionTime() != (CDouble)((unsigned long long)(Kernel->getProfile(i, j)->getExecutionTime())))
					out << Kernel->getProfile(i, j)->getExecutionTime();
				else
					out << ((unsigned long long)(Kernel->getProfile(i, j)->getExecutionTime()));

				if (j < Kernel->getNumberOfProfiles(i) - 1)
					out << ", " << Kernel->getProfile(i, j)->getWeight() << ")" << endl;
				else
					out << ", " << Kernel->getProfile(i, j)->getWeight() << ");" << endl;
			}

			out << "ExecutionTimes put(" << i + 1 << ", Distribution);" << endl;
		}

	} else
		out << endl << endl;

	out << "Status := new(ProcessMonitor) init(Monitor, MonitorID, Trace, Name);" << endl;
	out << "abort" << endl;
	out << "   par Fire()() and if Monitor then CheckAccuracyStatus()() fi rap" << endl;
	out << "with Monitor?StopSimulation; if Monitor then Monitor!Results(Status getResults) fi." << endl << endl;

	out << "CheckAccuracyStatus()() |ID: Integer|" << endl << endl;
	out << "Monitor?Status(ID | ID = MonitorID);" << endl;
	out << "Monitor!AccuracyStatus(MonitorID, Status accurate);" << endl;
	out << "CheckAccuracyStatus()()." << endl << endl;

	out << "Fire()() |Scenario: Integer|" << endl << endl;

	if (Kernel->hasControlInputChannels()) {
		out << "Control_" << Kernel->getControlInputChannels().front()->getName() << "!Inspect;" << endl;
		out << "Control_" << Kernel->getControlInputChannels().front()->getName() << "?Scenario(Scenario);" << endl;
	} else
		out << "Scenario := 0;" << endl;

	if (Kernel->getNumberOfScenarios() == 1) {

		for (CId j = 0; j != Kernel->getDataInputChannels().size(); j++)
			out << "In_" << Kernel->getDataInputChannels()[j]->getName() << "!Inspect("
				<< Kernel->getDataInputChannels()[j]->getConsumptionRate(0) << ");" << endl;

		for (CId j = 0; j != Kernel->getDataOutputChannels().size(); j++)
			out << "Out_" << Kernel->getDataOutputChannels()[j]->getName() << "!Room("
				<< Kernel->getDataOutputChannels()[j]->getProductionRate(0) << ");" << endl;

		for (CId j = 0; j != Kernel->getDataOutputChannels().size(); j++)
			out << "Out_" << Kernel->getDataOutputChannels()[j]->getName() << "!Reserve;" << endl;

		out << "Status start(currentTime);" << endl;
		
		if (Timed)
			out << "delay(ExecutionTimes get(Scenario + 1) sample);" << endl;

		out << "Status end(currentTime);" << endl;

		for (CId j = 0; j != Kernel->getDataInputChannels().size(); j++)
			out << "In_" << Kernel->getDataInputChannels()[j]->getName() << "!Read;" << endl;

		for (CId j = 0; j != Kernel->getDataOutputChannels().size(); j++)
			out << "Out_" << Kernel->getDataOutputChannels()[j]->getName() << "!Write;" << endl;
	}

    if (Kernel->getNumberOfScenarios() > 1) {

		for (CId i = 0; i != Kernel->getNumberOfScenarios(); i++) {

			bool TimedScenario = false;

			if (Timed)
				for (CId j = 0; !TimedScenario && j != Kernel->getNumberOfProfiles(i); j++)
					if (Kernel->getProfile(i, j)->getExecutionTime() != 0)
						TimedScenario = true;

			out << "if Scenario = " << i << " then" << endl;
				
			for (CId j = 0; j != Kernel->getDataInputChannels().size(); j++)
				if (Kernel->getDataInputChannels()[j]->getConsumptionRate(i) > 0)
					out << "   In_" << Kernel->getDataInputChannels()[j]->getName() << "!Inspect("
						<< Kernel->getDataInputChannels()[j]->getConsumptionRate(i) << ");" << endl;

			for (CId j = 0; j != Kernel->getDataOutputChannels().size(); j++)
				if (Kernel->getDataOutputChannels()[j]->getProductionRate(i) > 0)
					out << "   Out_" << Kernel->getDataOutputChannels()[j]->getName() << "!Room("
						<< Kernel->getDataOutputChannels()[j]->getProductionRate(i) << ");" << endl;

			for (CId j = 0; j != Kernel->getDataOutputChannels().size(); j++)
				if (Kernel->getDataOutputChannels()[j]->getProductionRate(i) > 0)
					out << "   Out_" << Kernel->getDataOutputChannels()[j]->getName() << "!Reserve;" << endl;

			out << "   Status start(currentTime);" << endl;
		
			if (TimedScenario)
				out << "   delay(ExecutionTimes get(Scenario + 1) sample);" << endl;
			
			out << "   Status end(currentTime)";

			for (CId j = 0; j != Kernel->getDataInputChannels().size(); j++)
				if (Kernel->getDataInputChannels()[j]->getConsumptionRate(i) > 0)
					out << ";" << endl << "   In_" << Kernel->getDataInputChannels()[j]->getName() << "!Read";

			for (CId j = 0; j != Kernel->getDataOutputChannels().size(); j++)
				if (Kernel->getDataOutputChannels()[j]->getProductionRate(i) > 0)
					out << ";" << endl << "   Out_" << Kernel->getDataOutputChannels()[j]->getName() << "!Write";

			out << endl << "fi;" << endl;
		}
	}

	if (Kernel->hasControlInputChannels())
		out << "Control_" << Kernel->getControlInputChannels()[0]->getName() << "!Read;" << endl;
	
	out << "Fire()()." << endl << endl;
}

void SADF2POOSL_Detector(SADF_Process* Detector, ostream &out) {

	bool Timed = false;

	for (CId i = 0; !Timed && i != Detector->getNumberOfSubScenarios(); i++)
		for (CId j = 0; !Timed && j != Detector->getNumberOfProfiles(i); j++)
			if (Detector->getProfile(i, j)->getExecutionTime() != 0)
				Timed = true;

	out << "process class " << Detector->getName() << "(Monitor: Boolean, MonitorID: Integer, Trace: Boolean, Name: String)" << endl;
	out << "instance variables" << endl;

	if (Timed)
		out << "ExecutionTimes: Array, ";

	out << "Status: ProcessMonitor, MarkovChains: Array" << endl << endl;
	out << "initial method call" << endl;
	out << "Initialise()()" << endl << endl;
	out << "instance methods" << endl;

	out << "Initialise()() |MarkovChain: MarkovChain";

	if (Timed) {
		
		out << ", Distribution: GeneralDiscrete|" << endl << endl;

		out << "ExecutionTimes := new(Array) size(" << Detector->getNumberOfSubScenarios() << ");" << endl;

		for(CId i = 0; i != Detector->getNumberOfSubScenarios(); i++) {

			out << "Distribution := new(GeneralDiscrete)" << endl;

			for (CId j = 0; j != Detector->getNumberOfProfiles(i); j++) {
	
				out << "   withParameters(";
			
				if (Detector->getProfile(i, j)->getExecutionTime() != (CDouble)((unsigned long long)(Detector->getProfile(i, j)->getExecutionTime())))
					out << Detector->getProfile(i, j)->getExecutionTime();
				else
					out << ((unsigned long long)(Detector->getProfile(i, j)->getExecutionTime()));

				if (j < Detector->getNumberOfProfiles(i) - 1)
					out << ", " << Detector->getProfile(i, j)->getWeight() << ")" << endl;
				else
					out << ", " << Detector->getProfile(i, j)->getWeight() << ");" << endl;
			}

			out << "ExecutionTimes put(" << i + 1 << ", Distribution);" << endl;
		}

	} else
		out << "|" << endl << endl;

	out << "MarkovChains := new(Array) size(" << Detector->getNumberOfScenarios() << ");" << endl;
		
	for (CId s = 0; s != Detector->getNumberOfScenarios(); s++) {
		
		out << "MarkovChain := new(MarkovChain) init" << endl;
		
		for (CId i = 0; i != Detector->getMarkovChain(s)->getNumberOfStates(); i++) {

			out << "   addState(\"" << Detector->getMarkovChain(s)->getStateName(i) << "\", "
				<< Detector->getMarkovChain(s)->getSubScenario(i) << ")" << endl;

			CDouble CumulativeWeight = 0;
			
			for (CId j = 0; j != Detector->getMarkovChain(s)->getNumberOfStates(); j++)

				if (Detector->getMarkovChain(s)->getTransitionProbability(i, j) != 0) {
					
					out << "      addTransition(\"" << Detector->getMarkovChain(s)->getStateName(i) << "\", \""
						<< Detector->getMarkovChain(s)->getStateName(j) << "\", " << CumulativeWeight << ", "
						<< CumulativeWeight + Detector->getMarkovChain(s)->getTransitionProbability(i, j) << ")" << endl;
				
					CumulativeWeight += Detector->getMarkovChain(s)->getTransitionProbability(i, j);
				}
		}
		
		out << "   setInitialState(\"" << Detector->getMarkovChain(s)->getStateName(Detector->getMarkovChain(s)->getInitialState()) << "\");" << endl;
		out << "MarkovChains put(" << s + 1 << ", MarkovChain);" << endl;
	}

	out << "Status := new(ProcessMonitor) init(Monitor, MonitorID, Trace, Name);" << endl;
	out << "abort" << endl;
	out << "   par Fire()() and if Monitor then CheckAccuracyStatus()() fi rap" << endl;
	out << "with Monitor?StopSimulation; if Monitor then Monitor!Results(Status getResults) fi." << endl << endl;

	out << "CheckAccuracyStatus()() |ID: Integer|" << endl << endl;
	out << "Monitor?Status(ID | ID = MonitorID);" << endl;
	out << "Monitor!AccuracyStatus(MonitorID, Status accurate);" << endl;
	out << "CheckAccuracyStatus()()." << endl << endl;

	out << "Fire()() |Scenario, SubScenario: Integer|" << endl << endl;

	if (Detector->hasControlInputChannels()) {
		out << "Control_" << Detector->getControlInputChannels().front()->getName() << "!Inspect;" << endl;
		out << "Control_" << Detector->getControlInputChannels().front()->getName() << "?Scenario(Scenario);" << endl;
	} else
		out << "Scenario := 0;" << endl;

	out << "SubScenario := MarkovChains get(Scenario + 1) getNextSubScenario;" << endl;

	if (Detector->getNumberOfSubScenarios() == 1) {

		for (CId j = 0; j != Detector->getDataInputChannels().size(); j++)
			out << "In_" << Detector->getDataInputChannels()[j]->getName() << "!Inspect("
				<< Detector->getDataInputChannels()[j]->getConsumptionRate(0) << ");" << endl;

		for (CId j = 0; j != Detector->getDataOutputChannels().size(); j++)
			out << "Out_" << Detector->getDataOutputChannels()[j]->getName() << "!Room("
				<< Detector->getDataOutputChannels()[j]->getProductionRate(0) << ");" << endl;

		for (CId j = 0; j != Detector->getControlOutputChannels().size(); j++)
			out << "Out_" << Detector->getControlOutputChannels()[j]->getName() << "!Room("
				<< Detector->getControlOutputChannels()[j]->getProductionRate(0) << ");" << endl;

		for (CId j = 0; j != Detector->getDataOutputChannels().size(); j++)
			out << "Out_" << Detector->getDataOutputChannels()[j]->getName() << "!Reserve;" << endl;

		for (CId j = 0; j != Detector->getControlOutputChannels().size(); j++)
			out << "Out_" << Detector->getControlOutputChannels()[j]->getName() << "!Reserve;" << endl;

		out << "Status start(currentTime);" << endl;
		
		if (Timed)
			out << "delay(ExecutionTimes get(SubScenario + 1) sample);" << endl;

		out << "Status end(currentTime);" << endl;

		for (CId j = 0; j != Detector->getDataInputChannels().size(); j++)
			out << "In_" << Detector->getDataInputChannels()[j]->getName() << "!Read;" << endl;

		for (CId j = 0; j != Detector->getDataOutputChannels().size(); j++)
			out << "Out_" << Detector->getDataOutputChannels()[j]->getName() << "!Write;" << endl;

		for (CId j = 0; j != Detector->getControlOutputChannels().size(); j++)
			out << "Out_" << Detector->getControlOutputChannels()[j]->getName() << "!Write("
				<< Detector->getControlOutputChannels()[j]->getProductionScenarioID(0) << ");" << endl;
	}

	if (Detector->getNumberOfSubScenarios() > 1) {

		for (CId i = 0; i != Detector->getNumberOfSubScenarios(); i++) {

			bool TimedScenario = false;

			if (Timed)
				for (CId j = 0; !TimedScenario && j != Detector->getNumberOfProfiles(i); j++)
					if (Detector->getProfile(i, j)->getExecutionTime() != 0)
						TimedScenario = true;
			
			out << "if SubScenario = " << i << " then" << endl;
				
			for (CId j = 0; j != Detector->getDataInputChannels().size(); j++)
				if (Detector->getDataInputChannels()[j]->getConsumptionRate(i) > 0)
					out << "   In_" << Detector->getDataInputChannels()[j]->getName() << "!Inspect("
						<< Detector->getDataInputChannels()[j]->getConsumptionRate(i) << ");" << endl;

			for (CId j = 0; j != Detector->getDataOutputChannels().size(); j++)
				if (Detector->getDataOutputChannels()[j]->getProductionRate(i) > 0)
					out << "   Out_" << Detector->getDataOutputChannels()[j]->getName() << "!Room("
						<< Detector->getDataOutputChannels()[j]->getProductionRate(i) << ");" << endl;

			for (CId j = 0; j != Detector->getControlOutputChannels().size(); j++)
				out << "   Out_" << Detector->getControlOutputChannels()[j]->getName() << "!Room("
					<< Detector->getControlOutputChannels()[j]->getProductionRate(i) << ");" << endl;

			for (CId j = 0; j != Detector->getDataOutputChannels().size(); j++)
				if (Detector->getDataOutputChannels()[j]->getProductionRate(i) > 0)
					out << "   Out_" << Detector->getDataOutputChannels()[j]->getName() << "!Reserve;" << endl;

			for (CId j = 0; j != Detector->getControlOutputChannels().size(); j++)
				out << "   Out_" << Detector->getControlOutputChannels()[j]->getName() << "!Reserve;" << endl;

			out << "   Status start(currentTime);" << endl;

			if (TimedScenario)
				out << "   delay(ExecutionTimes get(SubScenario + 1) sample);" << endl;

			out << "   Status end(currentTime)";

			for (CId j = 0; j != Detector->getDataInputChannels().size(); j++)
				if (Detector->getDataInputChannels()[j]->getConsumptionRate(i) > 0)
					out << ";" << endl << "   In_" << Detector->getDataInputChannels()[j]->getName() << "!Read";

			for (CId j = 0; j != Detector->getDataOutputChannels().size(); j++)
				if (Detector->getDataOutputChannels()[j]->getProductionRate(i) > 0)
					out << ";" << endl << "   Out_" << Detector->getDataOutputChannels()[j]->getName() << "!Write";

			for (CId j = 0; j != Detector->getControlOutputChannels().size(); j++)
				out << ";" << endl << "   Out_" << Detector->getControlOutputChannels()[j]->getName() << "!Write("
					<< Detector->getControlOutputChannels()[j]->getProductionScenarioID(i) << ")";

			out << endl << "fi;" << endl;
		}
	}

	if (Detector->hasControlInputChannels())
		out << "Control_" << Detector->getControlInputChannels()[0]->getName() << "!Read;" << endl;
		
	out << "Fire()()." << endl << endl;
}

void SADF2POOSL_Process(SADF_Graph* Graph, SADF_SimulationSettings* Settings, ostream &out) {

	for (CId i = 0; i != Graph->getNumberOfKernels(); i++)
        SADF2POOSL_Kernel(Graph->getKernel(i), out);
    
    for (CId i = 0; i != Graph->getNumberOfDetectors(); i++)
    	SADF2POOSL_Detector(Graph->getDetector(i), out);

	// Generate Process Class SimulationController

	out << "process class SimulationController(NumberOfMonitors: Integer, LogFile: String)" << endl;
	out << "instance variables" << endl;
	out << "Status: MonitorStatus" << endl << endl;
	out << "initial method call" << endl;
	out << "Initialise()()" << endl << endl;
	out << "instance methods" << endl;

	out << "Initialise()() |MonitorID: Integer, Results: String|" << endl << endl;
	out << "Status := new(MonitorStatus) init(NumberOfMonitors, LogFile);" << endl;

	if (Settings->getMaximumModelTime() != SADF_UNDEFINED) {
		if (Settings->getMaximumModelTime() != (CDouble)((unsigned long long)(Settings->getMaximumModelTime())))
			out << "abort CheckAccuracyStatus()() with delay(" << Settings->getMaximumModelTime() << ");" << endl;
		else
			out << "abort CheckAccuracyStatus()() with delay(" << ((unsigned long long)(Settings->getMaximumModelTime())) << ");" << endl;
	} else
		out << "CheckAccuracyStatus()();" << endl;

	out << "abort while true do Monitor!StopSimulation od with delay(1.0e-10);" << endl;

	out << "MonitorID := 1;" << endl;
	out << "while MonitorID <= NumberOfMonitors do" << endl;
	out << "   Monitor?Results(Results){Status append(Results)};" << endl;
	out << "   MonitorID := MonitorID + 1" << endl;
	out << "od;" << endl;
	out << "Status log." << endl << endl;

	out << "CheckAccuracyStatus()() |MonitorID: Integer, Accurate: Boolean|" << endl << endl;

	CDouble MaximumTimeStep = 0;
	CDouble MinimumTimeStep = -1;
	
	for (CId i = 0; i != Graph->getNumberOfKernels(); i++)
		for (CId j = 0; j != Graph->getKernel(i)->getNumberOfScenarios(); j++)
			for (CId k = 0; k != Graph->getKernel(i)->getNumberOfProfiles(j); k++) {
			
				if (Graph->getKernel(i)->getProfile(j, k)->getExecutionTime() > MaximumTimeStep)
					MaximumTimeStep = Graph->getKernel(i)->getProfile(j, k)->getExecutionTime();
					
				if (MinimumTimeStep == -1)
					MinimumTimeStep = Graph->getKernel(i)->getProfile(j, k)->getExecutionTime();
				else			
					if (Graph->getKernel(i)->getProfile(j, k)->getExecutionTime() < MinimumTimeStep)
						MinimumTimeStep = Graph->getKernel(i)->getProfile(j, k)->getExecutionTime();
			}

	for (CId i = 0; i != Graph->getNumberOfDetectors(); i++)
		for (CId j = 0; j != Graph->getDetector(i)->getNumberOfSubScenarios(); j++)
			for (CId k = 0; k != Graph->getDetector(i)->getNumberOfProfiles(j); k++) {
			
				if (Graph->getDetector(i)->getProfile(j, k)->getExecutionTime() > MaximumTimeStep)
					MaximumTimeStep = Graph->getDetector(i)->getProfile(j, k)->getExecutionTime();
					
				if (MinimumTimeStep == -1)
					MinimumTimeStep = Graph->getDetector(i)->getProfile(j, k)->getExecutionTime();
				else
					if (Graph->getDetector(i)->getProfile(j, k)->getExecutionTime() < MinimumTimeStep)
						MinimumTimeStep = Graph->getDetector(i)->getProfile(j, k)->getExecutionTime();
			}

	CDouble TimeStep = MaximumTimeStep - MinimumTimeStep;
	
	if (TimeStep != 0) {
		if (TimeStep != (CDouble)((CId)(TimeStep)))
			out << "delay(" << (50 * TimeStep) << ");" << endl;
		else
			out << "delay(" << (50 * ((CId)(TimeStep))) << ");" << endl;
	} else {
		if (MinimumTimeStep != (CDouble)((CId)(MinimumTimeStep)))
			out << "delay(" << (50 * MinimumTimeStep) << ");" << endl;
		else
			out << "delay(" << (50 * ((CId)(MinimumTimeStep))) << ");" << endl;
	}

	out << "MonitorID := 1;" << endl;
	out << "while MonitorID <= NumberOfMonitors do" << endl;
	out << "   Monitor!Status(MonitorID);" << endl;
	out << "   Monitor?AccuracyStatus(MonitorID, Accurate){Status register(MonitorID, Accurate)};" << endl;
	out << "   MonitorID := MonitorID + 1" << endl;
	out << "od;" << endl;
	out << "if (Status accurate not) then CheckAccuracyStatus()() fi." << endl << endl;

	// Generate Process Class DataBuffer
	
	out << "process class DataBuffer(BufferSize: Integer, InitialTokens: Integer, TokenSize: Integer, Monitor: Boolean, MonitorID: Integer, Trace: Boolean, Name: String)" << endl;
	out << "instance variables" << endl;
	out << "Status: DataBufferMonitor" << endl << endl;
	out << "initial method call" << endl;
	out << "Initialise()()" << endl << endl;
	out << "instance methods" << endl;
	
	out << "Initialise()()" << endl << endl;
	out << "Status := new(DataBufferMonitor) init(BufferSize, InitialTokens, TokenSize, Monitor, MonitorID, Trace, Name);" << endl;
	out << "abort" << endl;
	out << "   par HandleInput()() and HandleOutput()() and if Monitor then CheckAccuracyStatus()() fi rap" << endl;
	out << "with Monitor?StopSimulation; if Monitor then Monitor!Results(Status getResults) fi." << endl << endl;

	out << "CheckAccuracyStatus()() |ID: Integer|" << endl << endl;
	out << "Monitor?Status(ID | ID = MonitorID);" << endl;
	out << "Monitor!AccuracyStatus(MonitorID, Status accurate);" << endl;
	out << "CheckAccuracyStatus()()." << endl << endl;

	out << "HandleInput()() |NumberOfTokens: Integer|" << endl << endl;
	out << "In?Room(NumberOfTokens | Status room(NumberOfTokens));" << endl;
	out << "In?Reserve{Status reserve(NumberOfTokens, currentTime)};" << endl;
	out << "In?Write{Status write(NumberOfTokens)};" << endl;
	out << "HandleInput()()." << endl << endl;
	
	out << "HandleOutput()() |NumberOfTokens: Integer|" << endl << endl;
	out << "Out?Inspect(NumberOfTokens | Status available(NumberOfTokens));" << endl;
	out << "Out?Read{Status remove(NumberOfTokens, currentTime)};" << endl;
	out << "HandleOutput()()." << endl << endl;

	// Generate Process Class ControlBuffer
	
	out << "process class ControlBuffer(BufferSize: Integer, NumbersInitialTokens: Queue, ContentInitialTokens: Queue, TokenSize: Integer, Monitor: Boolean, MonitorID: Integer, Trace: Boolean, Name: String)" << endl;
	out << "instance variables" << endl;
	out << "Status: ControlBufferMonitor" << endl << endl;
	out << "initial method call" << endl;
	out << "Initialise()()" << endl << endl;
	out << "instance methods" << endl;
	
	out << "Initialise()()" << endl << endl;
	out << "Status := new(ControlBufferMonitor) init(BufferSize, NumbersInitialTokens, ContentInitialTokens, TokenSize, Monitor, MonitorID, Trace, Name);" << endl;
	out << "abort" << endl;
	out << "   par HandleInput()() and HandleOutput()() and if Monitor then CheckAccuracyStatus()() fi rap" << endl;
	out << "with Monitor?StopSimulation; if Monitor then Monitor!Results(Status getResults) fi." << endl << endl;

	out << "CheckAccuracyStatus()() |ID: Integer|" << endl << endl;
	out << "Monitor?Status(ID | ID = MonitorID);" << endl;
	out << "Monitor!AccuracyStatus(MonitorID, Status accurate);" << endl;
	out << "CheckAccuracyStatus()()." << endl << endl;

	out << "HandleInput()() |NumberOfTokens: Integer, Scenario: Integer|" << endl << endl;
	out << "In?Room(NumberOfTokens | Status room(NumberOfTokens));" << endl;
	out << "In?Reserve{Status reserve(NumberOfTokens, currentTime)};" << endl;
	out << "In?Write(Scenario){Status write(NumberOfTokens, Scenario)};" << endl;
	out << "HandleInput()()." << endl << endl;
	
	out << "HandleOutput()()" << endl << endl;
	out << "[Status available] Out?Inspect;" << endl;
	out << "Out!Scenario(Status inspect);" << endl;
	out << "Out?Read{Status remove(currentTime)};" << endl;
	out << "HandleOutput()()." << endl << endl;
}
