#!/usr/bin/env python3

from sdf_model_separated import Sdf_model_separated
# from sdf_model import Sdf_model

from sys import argv, stdout, stderr
import tempfile
import shutil
import subprocess
import os.path

from pprint import pprint

task_fname   = 'inputs/jpeg_task_graph.csv'
costs_fname  = 'inputs/platform_costs.csv'

sdf3_timeout = 2
# sdf3_path="../sdf3/build/release/Linux/bin/sdf3analysis-sdf"
sdf3_path = "../build/bin/throughput_analysis"

model = Sdf_model_separated(task_fname, costs_fname)
# model = Sdf_model(task_fname, costs_fname)

prefix = os.path.expanduser('~/dse/hipao/partitioning_dse/results/all_old_eval/')
if len(argv) > 1:
    prefix = argv[1]
header_name = prefix + 'header.txt'
objectives_name = prefix + 'objectives_all.dat'
genes_name = prefix + 'genes_all.dat'

exec_index = 0
tasks_order = None
with open(header_name, 'r') as header_file:
    lines = header_file.readlines()

    results_header = lines[0].split(';')
    exec_index = results_header.index('Execution time')

    tasks_order = [x.strip() for x in lines[1].strip(';\t\n').split(';')]
# pprint(tasks_order)
# print("Execution time is "+str(exec_index))

sorted_tasks = sorted(model.tasks_types, key=lambda x: model.tasks_types[x]+x)
old_order = dict(zip(tasks_order, range(len(tasks_order))))
new_task_order = [old_order[x] for x in sorted_tasks]
reorder_list = lambda l, order: [l[i]for i in order]

# t=[0 for x in tasks_order]
# t[2] = 2
# print(t)
# print(reorder_list(t, new_task_order))
# print(tasks_order)
# print(reorder_list(tasks_order, new_task_order))
# exit()
data = []
sdf_total_time = 0
with open(genes_name, 'r') as genes_file:
    with open(objectives_name, 'r') as objectives_file:
        g_line = genes_file.readline()
        o_line = objectives_file.readline()
        while g_line and o_line:
            objectives = [float(x) for x in o_line.split()]
            old_estimation = objectives[exec_index]

            genes = ['tile_sw' if x == '0' else 'tile_hw' for x in g_line.split()]
            mapping = dict(zip(tasks_order, genes))
            # pprint(mapping)
            model.set_mapping(mapping)

            temp_dir = tempfile.mkdtemp()
            xml_path = temp_dir+'/application.xml'
            model.save_to_file(xml_path)

            sdf3_time = float('NaN')
            sdf3_exec_time = sdf3_timeout
            try:
                sdf3_output = subprocess.check_output([
                    sdf3_path,
                    # "--graph",
                    xml_path,
                    # "--algo",
                    # "binding_aware_throughput(NSoC)"
                    ],
                    timeout=sdf3_timeout).decode('utf-8')

                sdf3_lines = sdf3_output.splitlines()
                # sdf3_time = 1/float(sdf3_lines[0].split()[1])
                sdf3_time = float(sdf3_lines[2].split()[1])
                # sdf3_time = 1/float(sdf3_lines[0].split('=')[1])
                sdf3_exec_time = sdf3_lines[1].split()[1]
                sdf3_exec_time = float(sdf3_exec_time.strip('ms'))

                # print(sdf3_exec_time, file=stderr)

                # print (sdf3_time)
                # print (sdf3_output)
            except subprocess.CalledProcessError:
                print("\tError in ", xml_path, file=stderr)
                destination = 'errors/unknown/'+os.path.basename(temp_dir)
                os.makedirs(destination)
                shutil.copy(xml_path, destination)
            except subprocess.TimeoutExpired:
                print("Timeout in ",xml_path, file=stderr)
                destination = 'errors/timeout/'+os.path.basename(temp_dir)
                os.makedirs(destination)
                shutil.copy(xml_path, destination)
            shutil.rmtree(temp_dir)

            stderr.write('{}ms\n'.format(sdf3_exec_time))
            # stderr.write('.')
            stderr.flush()

            # print(old_estimation, sdf3_time)
            data.append((
                old_estimation,
                sdf3_time,
                sdf3_time/old_estimation,
                g_line.strip()))
            sdf_total_time += sdf3_exec_time

            g_line = genes_file.readline()
            o_line = objectives_file.readline()

stderr.write('\n')

# for l in sorted(data):
#     print('\t'.join( map(str,l) ))
for l in sorted(data):
    numbers = l[0:-1]
    genes = l[-1].split()
    # print(genes)
    genes = reorder_list(genes, new_task_order)
    # print(genes)
    genes = ' '.join(genes)

    print('\t'.join( map(str,list(numbers)+[genes]) ))

for task in reorder_list(tasks_order, new_task_order):
    print(task, model.tasks_types[task])
    # print(task, model.tasks_types[task], file=stderr)

print('Average SDF3 processing time:', sdf_total_time/len(data), 'ms', file=stderr)

# print(tasks_order)
# print(reorder_list(tasks_order, new_task_order))
# print(new_task_order)
