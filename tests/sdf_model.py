#!/usr/bin/env python3

class Sdf_model(object):
    from sys import argv, stdout
    HW = 0
    SW = 1

    def __init__(self, graph_fname, cost_fname):
        self.process_cost_file(cost_fname)
        self.process_graph_file(graph_fname)

        actor_description = '\n'.join([self.get_actor_str(task) for task in self.tasks_types.keys()])
        channel_str = self.get_channel_str()
        actor_props = self.get_actor_properties()

        self.application_graph = self.application_graph_template.format(
            actors=actor_description,
            channels=channel_str,
            actor_properties=actor_props)

        # Get the architecture file
        self.arch_str = ''
        with open('inputs/platform_definition.xml', 'r') as arch_file:
            self.arch_str = arch_file.read()

        self.mapping = {}

    def process_cost_file(self, cost_fname):
        self.cost_table = {}
        with open(cost_fname, 'r') as costs_file:
            for line in costs_file:
                if line[0] == '\n' or line[0] == '#':
                    continue
                parts = [x.strip() for x in line.split(';')]
                type_name = parts[0]
                data = [float(x) for x in parts[1:]]
                data[1] *= 1e3

                time = data[0:2]
                cost = data[2:4]
                power = data[4:6]
                self.cost_table[type_name] = {'time':time, 'cost': cost, 'power':power}

    def process_graph_file(self, graph_fname):
        self.tasks_connections_in = {}
        self.tasks_connections_out = {}
        self.tasks_types = {}
        self.connections = {}

        # Reading the task graph
        with open(graph_fname,'r') as graph_file:
            conn_count = 0
            for line in graph_file:
                if line[0] == '\n' or line[0] == '#' or 'Part class' in line:
                    continue
                parts = [x.strip() for x in line.split(";")]
                src_name = parts[0]
                src_class = parts[1]
                src_port = "out"+str(conn_count)
                # src_port = parts[2]
                dst_name = parts[5]
                dst_port = "in"+str(conn_count)
                # dst_port = parts[6]
                dst_class = parts[7]
                conn_count+=1

                if parts[4] == "OUT" and dst_name != '':
                    self.connections[src_name+'_to_'+dst_name] = (src_name, src_port, dst_name, dst_port)
                    if src_name not in self.tasks_types:
                        self.tasks_types[src_name] = src_class
                    try:
                        self.tasks_connections_out[src_name].append((src_port, dst_port, dst_name))
                    except KeyError:
                        self.tasks_connections_out[src_name] = [(src_port, dst_port, dst_name)]

                    if dst_name not in self.tasks_types:
                        self.tasks_types[dst_name] = dst_class
                    try:
                        self.tasks_connections_in[dst_name].append((dst_port, src_port, src_name))
                    except KeyError:
                        self.tasks_connections_in[dst_name] = [(dst_port, src_port, src_name)]

    def get_actor_ports_str(self, task):
        actor_port_template = """
    <port name="{port_name}" type="{port_direction}" rate="1" />"""
        port_str = ''
        try:
            in_ports = [x[0] for x in self.tasks_connections_in[task]]
            port_str += ''.join([
                actor_port_template.format(port_name=x, port_direction='in')
                for x in in_ports])
        except KeyError:
            pass
        try:
            out_ports = [x[0] for x in self.tasks_connections_out[task]]
            port_str += ''.join([
                actor_port_template.format(port_name=x, port_direction='out')
                for x in out_ports])
        except KeyError:
            pass

        return port_str

    def get_actor_str(self, task):
        actor_template = (
            '<actor name="{actor_name}" type="{actor_type}">'
            '{ports_string}\n'
            '</actor>')
        return actor_template.format(
            actor_name=task,
            actor_type=self.tasks_types[task],
            ports_string=self.get_actor_ports_str(task))

    def get_channel_str(self):
        channel_template = (
            '<channel name="{channel_name}" '
            'srcActor="{src_actor}" srcPort="{src_port}" '
            'dstActor="{dst_actor}" dstPort="{dst_port}" />')
        channels = []
        for conn_name, conn in self.connections.items():
            channels.append(channel_template.format(
                channel_name=conn_name,
                src_actor=conn[0],
                src_port=conn[1],
                dst_actor=conn[2],
                dst_port=conn[3]))
        return '\n'.join(channels)

    def get_actor_properties(self):
        actor_properties_template = (
            '<actorProperties actor="{actor_name}">\n'
            '    <processor type="sw" default="true">\n'
            '        <executionTime time="{sw_time}" />\n'
            '    </processor>\n'
            '    <processor type="hw">\n'
            '        <executionTime time="{hw_time}" />\n'
            '    </processor>\n'
            '</actorProperties>')

        props_str = []
        for task,task_type in self.tasks_types.items():
            time = self.cost_table[task_type]['time']
            props_str.append(
                actor_properties_template.format(
                    actor_name=task,
                    sw_time=time[self.SW],
                    hw_time=time[self.HW]))
        return '\n'.join(props_str)

    application_graph_template = (
        '<applicationGraph name="jpegEncoder">\n'
        '<sdf name="jpegEncoder" type="JpegEncoder">\n'
        '    {actors}\n'
        '    {channels}\n'
        '</sdf>\n'
        '<sdfProperties>\n'
        '    {actor_properties}\n'
        '</sdfProperties>\n'
        '</applicationGraph>\n')

    # Mapping part
    mapping_template = (
        '<mapping appGraph="jpegEncoder" archGraph="arch">\n'
        '    {tile_mappings}\n'
        '</mapping>')
    tile_mapping_template = (
        '<tile name="{tile_name}">\n'
        '    <processor name="{proc_name}" timeslice="1">\n'
        '        {processor_actors}\n'
        '        <schedule>\n'
        '            {processor_schedule}\n'
        '        </schedule>\n'
        '    </processor>\n'
        '    <memory name="mem">\n'
        '        {memory_actors}\n'
        '        {memory_channels}\n'
        '    </memory>\n'
        '    <networkInterface name="ni">\n'
        '        {network_channels}\n'
        '    </networkInterface>\n'
        '</tile>')

    # Find mapping
    tile_list = ['tile_sw', 'tile_hw']
    def load_mapping_from_file(self, fname):
        with open(fname) as map_file:
            for line in map_file:
                task, _, tile = line.split()
                self.mapping[task] = tile

    def default_mapping(self):
        for task in self.tasks_types.keys():
            self.mapping[task] = "tile_sw"

    def get_task_list(self):
        return self.tasks_types.keys()

    def set_mapping(self, new_map):
        assert(set(new_map).issuperset(set(self.mapping)))
        self.mapping = new_map

    def is_sw(self,tile):
        return tile == "tile_sw"

    def generate_schedule(self):
        "Generate a reasonable (FIFO) scheduling order"
        # Find tasks without inputs (sources)
        task_pool = set(self.tasks_types.keys())
        source_tasks = task_pool.difference( set(self.tasks_connections_in.keys()) )

        from collections import deque
        task_order = []
        ready_tasks = deque(source_tasks)
        while len(ready_tasks) > 0:
            task = ready_tasks.popleft()
            task_order.append(task)
            try:
                for connection in self.tasks_connections_out[task]:
                    ready_tasks.append(connection[2])
            except KeyError:
                pass
        return task_order

    def get_mapping(self):
        sched = self.generate_schedule()
        tiles_description=[]
        connections_map = {}
        for tile in self.tile_list:
            processor_actors=[]
            memory_actors=[]
            memory_channels=set()
            network_channels = set()
            for task, mapped_tile in self.mapping.items():
                if mapped_tile == tile:
                    processor_actors.append('<actor name="{}" />'.format(task))
                    memory_cost = 0
                    if self.is_sw(tile):
                        memory_cost = self.cost_table[self.tasks_types[task]]['cost'][self.SW]
                    memory_actors.append('<actor name="{}" size="{}" />'.format(
                        task,
                        memory_cost
                        ))
                    for channel_name, c in self.connections.items():
                        if c[0] == task or  c[2] == task:
                            memory_channels.add(channel_name)
                        if self.mapping[c[0]] != self.mapping[c[2]]:
                            network_channels.add(channel_name)
                            conn_name = self.mapping[c[0]] + "_to_" + self.mapping[c[2]]
                            try:
                                connections_map[conn_name].add(channel_name)
                            except KeyError:
                                connections_map[conn_name] = set((channel_name,))

            def filter_schedule(sched, tile):
                return filter(lambda x: self.mapping[x] == tile, sched)

            scheduled_tasks = list(filter_schedule(sched, tile))
            schedule_str = ''
            if len(scheduled_tasks) > 0:
                schedule_str = '<state actor="{}" startOfPeriodicRegime="true"/>'.format(scheduled_tasks[0])
                if len(scheduled_tasks) > 1:
                    schedule_str += ''.join([
                        '\n<state actor="{}" />'.format(x)
                        for x in scheduled_tasks[1:]])
            p_name = tile.split('_')[1]
            tiles_description.append(self.tile_mapping_template.format(
                tile_name=tile,
                proc_name=p_name,
                processor_actors='\n'.join(processor_actors),
                processor_schedule=schedule_str,
                memory_actors= '\n'.join(memory_actors),
                memory_channels= '\n'.join(['<channel name="{}" size="128" />'.format(x) for x in memory_channels]),
                network_channels= '\n'.join(
                    ['<channel name="{}" size="128" inBandwidth="1" outBandwidth="1" nrConnections="1" />'.format(x)
                    for x in network_channels]),
                ))
        connection_str = ''
        for c_name, channels in connections_map.items():
            connection_str += '\n<connection name="{}">\n'.format(c_name)
            connection_str += ('\n'.join([
                '<channel name="{}" size="128" inBandwidth="1" outBandwidth="1" nrConnections="1"/>'.format(x)
                for x in channels]))
            connection_str += '\n</connection>'
        return '\n'.join(tiles_description) + connection_str

    def save_to_file(self, fname):
        file_template = (
            '<?xml version="1.0" encoding="UTF-8"?>\n'
            '<sdf3 type="sdf" version="1.0" '
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            'xsi:noNamespaceSchemaLocation="http://www.es.ele.tue.nl/sdf3/xsd/sdf3-sdf.xsd">\n'
            '{application_graph}'
            '{architecture_graph}'
            '{mapping}'
            '</sdf3>')
        mapping_str = self.mapping_template.format(
            tile_mappings=self.get_mapping())
        output_data = file_template.format(
            application_graph=self.application_graph,
            architecture_graph=self.arch_str,
            mapping=mapping_str)
        with smart_open(fname, 'w') as f:
            f.write(output_data)

import sys
import contextlib

@contextlib.contextmanager
def smart_open(filename=None, *args, **kwargs):
    if filename and filename != '-':
        fh = open(filename, 'w')
    else:
        fh = sys.stdout

    try:
        yield fh
    finally:
        if fh is not sys.stdout:
            fh.close()

if __name__ == '__main__':
    from pprint import pprint
    # Test the platform
    graph_fname = 'inputs/jpeg_task_graph.csv'
    cost_fname = 'inputs/platform_costs.csv'
    model = Sdf_model(graph_fname, cost_fname)
    # print(model.get_platform())

    # model.default_mapping()
    model.set_mapping({
        'part10': 'tile_sw',
        'part17': 'tile_sw',
        'part3': 'tile_sw',
        'part12': 'tile_sw',
        'part19': 'tile_sw',
        'part5': 'tile_sw',
        'part2': 'tile_sw',
        'part11': 'tile_hw',
        'part18': 'tile_sw',
        'part4': 'tile_sw',
        'part14': 'tile_sw',
        'part21': 'tile_sw',
        'part7': 'tile_sw',
        'part15': 'tile_sw',
        'part16': 'tile_sw',
        'part22': 'tile_sw',
        'part23': 'tile_sw',
        'part8': 'tile_sw',
        'part9': 'tile_sw',
        'part13': 'tile_sw',
        'part20': 'tile_sw',
        'part6': 'tile_sw'
})

    model.save_to_file('-')
