#!/usr/bin/env python3

import tempfile
import shutil
import subprocess
import os.path

from sdf_model_separated import Sdf_model_separated

sdf3_path = "../build/bin/throughput_analysis"
sdf3_timeout = 1

def sequence_test():
    test_name = "Sequence "
    task_fname = "test_inputs/sequence_app.csv"
    costs_fname = "test_inputs/sequence_platform.csv"

    model = Sdf_model_separated(task_fname, costs_fname)
    model.channel_latency = 0
    tasks = model.get_task_list()
    # All in HW sould be = 9
    mapping = dict(zip(tasks, ['tile_hw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    print(test_name,"all HW ",t)
    print("\tShould be 9")

    # All in SW sould be = 18000
    mapping = dict(zip(tasks, ['tile_sw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    print(test_name,"all SW ",t)
    print("\tShould be 18000")

def bifurcation_test():
    test_name = "Bifurcation "
    task_fname = "test_inputs/bifurcation_app.csv"
    costs_fname = "test_inputs/bifurcation_platform.csv"

    model = Sdf_model_separated(task_fname, costs_fname)
    model.channel_latency = 0
    tasks = model.get_task_list()
    # All in HW sould be = 13
    mapping = dict(zip(tasks, ['tile_hw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    print(test_name,"all HW ",t)
    print("\tShould be 13")

    # All in SW sould be = 32000
    mapping = dict(zip(tasks, ['tile_sw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    print(test_name,"all SW ",t)
    print("\tShould be 32000")

def long_branches_seq_test():
    test_name = "Long branches seq "
    task_fname = "test_inputs/long_branches_seq_app.csv"
    costs_fname = "test_inputs/long_branches_platform.csv"

    model = Sdf_model_separated(task_fname, costs_fname)
    model.channel_latency = 0
    tasks = model.get_task_list()
    # All in HW sould be = 14
    mapping = dict(zip(tasks, ['tile_hw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    model.save_to_file('/tmp/hardware_seq.xml')
    print(test_name,"all HW ",t)
    print("\tShould be 14")

    # All in SW sould be = 22000
    mapping = dict(zip(tasks, ['tile_sw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    print(test_name,"all SW ",t)
    print("\tShould be 22000")

def long_branches_par_test():
    test_name = "Long branches par "
    task_fname = "test_inputs/long_branches_par_app.csv"
    costs_fname = "test_inputs/long_branches_platform.csv"

    model = Sdf_model_separated(task_fname, costs_fname)
    model.channel_latency = 0
    tasks = model.get_task_list()
    # All in HW sould be = 11
    mapping = dict(zip(tasks, ['tile_hw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    model.save_to_file('/tmp/hardware_par.xml')
    print(test_name,"all HW ",t)
    print("\tShould be 11")

    # All in SW sould be = 22000
    mapping = dict(zip(tasks, ['tile_sw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    print(test_name,"all SW ",t)
    print("\tShould be 22000")

def generic_test(test_name, task_fname, costs_fname, ref_hw, ref_sw):
    test_name = "Long branches par "
    task_fname = "test_inputs/long_branches_par_app.csv"
    costs_fname = "test_inputs/long_branches_platform.csv"

    model = Sdf_model_separated(task_fname, costs_fname)
    model.channel_latency = 0
    tasks = model.get_task_list()

    # All in HW
    mapping = dict(zip(tasks, ['tile_hw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    model.save_to_file('/tmp/hardware_par.xml')
    print(test_name,"all HW ",t)
    print("\tShould be ", ref_hw)

    # All in SW
    mapping = dict(zip(tasks, ['tile_sw']*len(tasks)))
    model.set_mapping(mapping)
    t = evaluate_throughput(model)
    print(test_name,"all SW ",t)
    print("\tShould be ", ref_sw)


def evaluate_throughput(model):
    temp_dir = tempfile.mkdtemp()
    xml_path = temp_dir+'/application.xml'
    model.save_to_file(xml_path)

    sdf3_time = float('NaN')
    # sdf3_exec_time = sdf3_timeout
    try:
        sdf3_output = subprocess.check_output([
            sdf3_path,
            # "--graph",
            xml_path,
            # "--algo",
            # "binding_aware_throughput(NSoC)"
            ],
            timeout=sdf3_timeout).decode('utf-8')

        sdf3_lines = sdf3_output.splitlines()
        sdf3_time = 1/float(sdf3_lines[0].split()[1])
        # sdf3_time = 1/float(sdf3_lines[0].split('=')[1])
        # sdf3_exec_time = sdf3_lines[1].split()[1]
        # sdf3_exec_time = float(sdf3_exec_time.strip('ms'))

        # print(sdf3_exec_time, file=stderr)

        # print (sdf3_time)
        # print (sdf3_output)
    except subprocess.CalledProcessError:
        print("\tError in ", xml_path)
        # destination = 'errors/unknown/'+os.path.basename(temp_dir)
        # os.makedirs(destination)
        # shutil.copy(xml_path, destination)
    except subprocess.TimeoutExpired:
        print("Timeout in ",xml_path)
        # destination = 'errors/timeout/'+os.path.basename(temp_dir)
        # os.makedirs(destination)
        # shutil.copy(xml_path, destination)
    except:
        print(sdf3_output)
    shutil.rmtree(temp_dir)
    return sdf3_time

if __name__ == "__main__":
    sequence_test()
    bifurcation_test()
    long_branches_seq_test()
    long_branches_par_test()
    generic_test("New test ", "test_inputs/new_app.csv", "test_inputs/new_platform.csv", 0,0)
