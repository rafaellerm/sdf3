#!/usr/bin/env python3

from sys import argv, stdout

HW = 0
SW = 1

# Reading the cost table
cost_table = {}
with open(argv[2], 'r') as costs_file:
    for line in costs_file:
        if line[0] == '\n' or line[0] == '#':
            continue
        parts = [x.strip() for x in line.split(';')]
        type_name = parts[0]
        data = [float(x) for x in parts[1:]]
        data[1] *= 1e3

        time = data[0:2]
        cost = data[2:4]
        power = data[4:6]
        cost_table[type_name] = {'time':time, 'cost': cost, 'power':power}

tasks_connections_in = {}
tasks_connections_out = {}
tasks_types = {}
connections = {}

# Reading the task graph
with open(argv[1],'r') as graph_file:
    conn_count = 0
    for line in graph_file:
        if line[0] == '\n' or line[0] == '#' or 'Part class' in line:
            continue
        parts = [x.strip() for x in line.split(";")]
        src_name = parts[0]
        src_class = parts[1]
        src_port = "out"+str(conn_count)
        # src_port = parts[2]
        dst_name = parts[5]
        dst_port = "in"+str(conn_count)
        # dst_port = parts[6]
        dst_class = parts[7]
        conn_count+=1

        if parts[4] == "OUT" and dst_name != '':
            connections[src_name+'_to_'+dst_name] = (src_name, src_port, dst_name, dst_port)
            if src_name not in tasks_types:
                tasks_types[src_name] = src_class
            try:
                tasks_connections_out[src_name].append((src_port, dst_port, dst_name))
            except KeyError:
                tasks_connections_out[src_name] = [(src_port, dst_port, dst_name)]

            if dst_name not in tasks_types:
                tasks_types[dst_name] = dst_class
            try:
                tasks_connections_in[dst_name].append((dst_port, src_port, src_name))
            except KeyError:
                tasks_connections_in[dst_name] = [(dst_port, src_port, src_name)]


def get_actor_ports_str(task):
    actor_port_template = """
    <port name="{port_name}" type="{port_direction}" rate="1" />"""
    port_str = ''
    try:
        in_ports = [x[0] for x in tasks_connections_in[task]]
        port_str += ''.join([
            actor_port_template.format(port_name=x, port_direction='in')
            for x in in_ports])
    except KeyError:
        pass
    try:
        out_ports = [x[0] for x in tasks_connections_out[task]]
        port_str += ''.join([
            actor_port_template.format(port_name=x, port_direction='out')
            for x in out_ports])
    except KeyError:
        pass

    return port_str

def get_actor_str(task):
    actor_template = '<actor name="{actor_name}" type="{actor_type}">{ports_string}\n\
</actor>'
    return actor_template.format(
        actor_name=task,
        actor_type=tasks_types[task],
        ports_string=get_actor_ports_str(task))

def get_channel_str():
    channel_template = '\
<channel name="{channel_name}" \
srcActor="{src_actor}" srcPort="{src_port}" \
dstActor="{dst_actor}" dstPort="{dst_port}" />'
    channels = []
    for conn_name, conn in connections.items():
        channels.append(channel_template.format(
            channel_name=conn_name,
                src_actor=conn[0],
                src_port=conn[1],
                dst_actor=conn[2],
                dst_port=conn[3]))
    return '\n'.join(channels)

def get_actor_properties():
    actor_properties_template = """<actorProperties actor="{actor_name}">
    <processor type="sw" default="true">
        <executionTime time="{sw_time}" />
    </processor>
    <processor type="hw">
        <executionTime time="{hw_time}" />
    </processor>
</actorProperties>"""

    props_str = []
    for task,task_type in tasks_types.items():
        time = cost_table[task_type]['time']
        props_str.append(
            actor_properties_template.format(
                actor_name=task,
                sw_time=time[SW],
                hw_time=time[HW]))
    return '\n'.join(props_str)

application_graph_template = """<applicationGraph name="jpegEncoder">
    <sdf name="jpegEncoder" type="JpegEncoder">
        {actors}
        {channels}
    </sdf>
    <sdfProperties>
        {actor_properties}
    </sdfProperties>
</applicationGraph>"""

file_template = """<?xml version="1.0" encoding="UTF-8"?>
<sdf3 type="sdf" version="1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="http://www.es.ele.tue.nl/sdf3/xsd/sdf3-sdf.xsd">
    {application_graph}
    {architecture_graph}
    {mapping}
</sdf3>
"""

actor_description = '\n'.join([get_actor_str(task) for task in tasks_types.keys()])
channel_str = get_channel_str()
actor_props = get_actor_properties()


application_graph = application_graph_template.format(
    actors=actor_description,
    channels=channel_str,
    actor_properties=actor_props)

# Get the architecture file
arch_str = ''
with open('inputs/platform_definition.xml', 'r') as arch_file:
    arch_str = arch_file.read()


# Mapping part
mapping_template = '<mapping appGraph="jpegEncoder" archGraph="arch">\n\
    {tile_mappings}\n\
</mapping>'
tile_mapping_template = """
<tile name="{tile_name}">
    <processor name="sw" timeslice="1000">
        {processor_actors}
        <schedule>
            {processor_schedule}
        </schedule>
    </processor>
    <memory name="mem">
        {memory_actors}
        {memory_channels}
    </memory>
    <networkInterface name="ni">
        {network_channels}
    </networkInterface>
</tile>"""

# Find mapping
tile_list = ['tile_sw', 'tile_hw']
mapping = {}
if len(argv) > 3:
    with open(argv[3]) as map_file:
        for line in map_file:
            task, _, tile = line.split()
            mapping[task] = tile
else:
    for task in tasks_types.keys():
        mapping[task] = "tile_sw"

def is_sw(tile):
    return tile == "tile_sw"
# Generate a reasonable (FIFO) scheduling order
def generate_schedule():
    # Find tasks without inputs (sources)
    from collections import deque
    task_pool = set(tasks_types.keys())
    source_tasks = task_pool.difference( set(tasks_connections_in.keys()) )
    task_order = []
    ready_tasks = deque(source_tasks)
    while len(ready_tasks) > 0:
        task = ready_tasks.popleft()
        task_order.append(task)
        try:
            for connection in tasks_connections_out[task]:
                ready_tasks.append(connection[2])
        except KeyError:
            pass
    return task_order

def filter_schedule(sched, tile):
    return filter(lambda x: mapping[x] == tile, sched)

# print(generate_schedule())

def get_mapping():
    sched = generate_schedule()
    tiles_description=[]
    connections_map = {}
    for tile in tile_list:
        processor_actors=[]
        memory_actors=[]
        memory_channels=set()
        network_channels = set()
        for task, mapped_tile in mapping.items():
            if mapped_tile == tile:
                processor_actors.append('<actor name="{}" />'.format(task))
                memory_cost = 0
                if is_sw(tile):
                    memory_cost = cost_table[tasks_types[task]]['cost'][SW]
                memory_actors.append('<actor name="{}" size="{}" />'.format(
                    task,
                    memory_cost
                    ))
                for channel_name, c in connections.items():
                    if c[0] == task or  c[2] == task:
                        memory_channels.add(channel_name)
                    if mapping[c[0]] != mapping[c[2]]:
                        network_channels.add(channel_name)
                        conn_name = mapping[c[0]] + "_to_" + mapping[c[2]]
                        try:
                            connections_map[conn_name].add(channel_name)
                        except KeyError:
                            connections_map[conn_name] = set((channel_name,))

        scheduled_tasks = list(filter_schedule(sched, tile))
        schedule_str = ''
        if len(scheduled_tasks) > 0:
            schedule_str = '<state actor="{}" startOfPeriodicRegime="true"/>'.format(scheduled_tasks[0])
            if len(scheduled_tasks) > 1:
                schedule_str += ''.join([
                    '\n<state actor="{}" />'.format(x)
                    for x in scheduled_tasks[1:]])
        tiles_description.append(tile_mapping_template.format(
            tile_name=tile,
            processor_actors='\n'.join(processor_actors),
            processor_schedule=schedule_str,
            memory_actors= '\n'.join(memory_actors),
            memory_channels= '\n'.join(['<channel name="{}" size="128" />'.format(x) for x in memory_channels]),
            network_channels= '\n'.join(
                ['<channel name="{}" size="128" inBandwidth="1" outBandwidth="1" nrConnections="1" />'.format(x)
                for x in network_channels]),
            ))
    connection_str = ''
    for c_name, channels in connections_map.items():
        connection_str += '\n<connection name="{}">\n'.format(c_name)
        connection_str += ('\n'.join([
            '<channel name="{}" size="128" inBandwidth="1" outBandwidth="1" nrConnections="1"/>'.format(x)
            for x in channels]))
        connection_str += '\n</connection>'
    return '\n'.join(tiles_description) + connection_str

mapping_str = mapping_template.format(
    tile_mappings=get_mapping())
# print(mapping_str)


output_data = file_template.format(
    application_graph=application_graph,
    architecture_graph=arch_str,
    mapping=mapping_str)

# print(actor_description)
# print(channel_str)
# print(actor_props)
# print(application_graph)

for task,task_type in sorted(tasks_types.items(), key=lambda x: x[1]):
    print(task+'\t'+task_type+'\t'+mapping[task])

output_file = stdout
if len(argv) > 4:
    output_file = open(argv[4], 'w')
output_file.write(output_data)

output_file.close()
