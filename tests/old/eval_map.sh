#!/bin/sh

TMP_FILE=/tmp/sdf3graph
# mkfifo $TMP_FILE
./generate_xml.py inputs/jpeg_task_graph.csv inputs/platform_costs.csv $1 $TMP_FILE
../sdf3/build/release/Linux/bin/sdf3analysis-sdf --graph $TMP_FILE --algo 'binding_aware_throughput(MPFlow)'
# rm $TMP_FILE