#!/usr/bin/env python3

from sys import argv

repetitions_base = 3
repetitions_specific = {'VLC':6}

template = \
"""<actorProperties actor="{name}">
	<processor type="sw" default="true">
		<executionTime time="{sw_time}" />
	</processor>
	<processor type="hw">
		<executionTime time="{hw_time}" />
	</processor>
</actorProperties>"""

with open(argv[1]) as file:
	for line in file:
		parts = line.split(';')
		part_name = parts[0]
		data = [float(x) for x in parts[1:]]
		data[1] *= 1e3

		n_copies = repetitions_base
		if part_name in repetitions_specific:
			n_copies = repetitions_specific[part_name]
		for i in range(1,n_copies+1):
			print(template.format(
				name=part_name+str(i), 
				sw_time=data[1],
				hw_time=data[0]))