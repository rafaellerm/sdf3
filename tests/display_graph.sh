#! /bin/sh

../build/bin/sdf3print --graph $1 --format dot |
dot -T png |
display
