#!/bin/sh

# cat mapping_graph.dot |
# ../build/bin/sdf_to_html $1
sed 's/^.*\[.*tile.*to.*\].*$//g' mapping_graph.dot |
sed 's/^.*size.*$//g' |
dot -Tsvg | display -