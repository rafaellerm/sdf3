#!/bin/sh

# cat mapping_graph.dot |
# ../build/bin/sdf_to_html $1
sed 's/\[.*tile.*to.*\]//g' platform_graph.dot |
sed 's/^.*size.*$//g' |
dot -Kcirco -Lg -Tsvg | display