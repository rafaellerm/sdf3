/*
 *  TU Eindhoven
 *  Eindhoven, The Netherlands
 *
 *  Name            :   sdf3analysis.cc
 *
 *  Author          :   Sander Stuijk (sander@ics.ele.tue.nl)
 *
 *  Date            :   April 23, 2007
 *
 *  Function        :   SDFG analysis algorithms
 *
 *  History         :
 *      23-04-07    :   Initial version.
 *
 * $Id: sdf3analysis.cc,v 1.10 2009-12-23 13:32:08 sander Exp $
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * In other words, you are welcome to use, share and improve this program.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them.   Happy coding!
 */

#include "sdf3analysis.h"
#include <sdf/sdf.h>
#include <sdf/output/html/html.h>

 using namespace MaxPlusAnalysis;

 typedef struct _CPair
 {
    CString key;
    CString value;
} CPair;

typedef list<CPair>         CPairs;
typedef CPairs::iterator    CPairsIter;

/**
 * Settings
 * Struct to store program settings.
 */
 typedef struct _Settings
 {
    // Input file with graph
    CString graphFile;

    // Output file
    CString outputFile;

    // Application graph
    CNode *xmlAppGraph;

    // Architecture graph
    CNode *xmlArchGraph;

    // Mapping
    CNode *xmlMapping;

    CDoc *graphDoc;
} Settings;

/**
 * settings
 * Program settings.
 */
 Settings settings;

 CNode * loadFile(CString &file, CString module)
 {
    CNode *sdf3Node;
    CDoc *graphDoc;

    // Open file
    graphDoc = CParseFile(file);
    if (graphDoc == NULL)
        throw CException("Failed loading application from '" + file + "'.");

    // Locate the sdf3 root element and check module type
    sdf3Node = CGetRootNode(graphDoc);
    if (CGetAttribute(sdf3Node, "type") != module)
    {
        throw CException("Root element in file '" + file + "' is not "
           "of type '" + module + "'.");
    }

    settings.graphDoc = graphDoc;
    return sdf3Node;
}

/**
 * initSettings ()
 * The function initializes the program settings.
 */
 void initSettings(int argc, char **argv)
 {
    // Parse the command line
    if (argc == 1)
    {
        throw CException("");
    }
    settings.graphFile = argv[1];

    // Check required settings
    if (settings.graphFile.empty())
    {
        // helpMessage(cerr);
        throw CException("");
    }

    // Load XML file
    CNode *sdf3Node = loadFile(settings.graphFile, MODULE);

    settings.xmlAppGraph = CGetChildNode(sdf3Node, "applicationGraph");
    if (settings.xmlAppGraph == NULL)
        throw CException("No application graph in '" + settings.graphFile + "'.");

    settings.xmlArchGraph = CGetChildNode(sdf3Node, "architectureGraph");

    settings.xmlMapping = CGetChildNode(sdf3Node, "mapping");
}

// void outputHtml(TimedSDFgraph *g, /*CPairs &analyze, */ostream &out)
// {
//     CTimer timer;

//     SDFflowType flowType;
//     BindingAwareSDFG *bindingAwareSDFG;
//     PlatformGraph *platformGraph;
//     SDFstateSpaceBindingAwareThroughputAnalysis thrBindingAlgo;
//     vector<double> tileUtilization;
//     double thr;

//     flowType = SDFflowTypeNSoC;
//     // flowType = SDFflowTypeMPFlow;

//     // Create a platform graph
//     if (settings.xmlArchGraph == NULL)
//         throw CException("No architectureGraph given.");
//     platformGraph = constructPlatformGraph(settings.xmlArchGraph);

//     // Set the mapping of the application onto the platform graph
//     if (settings.xmlMapping == NULL)
//         throw CException("No mapping given.");
//     setMappingPlatformGraph(platformGraph, g, settings.xmlMapping);

//     // Create a binding-aware SDFG
//     bindingAwareSDFG = new BindingAwareSDFG(g, platformGraph, flowType);

//     // Measure execution time
//     startTimer(&timer);

//     // Analayze the throughput
//     thr = thrBindingAlgo.analyze(bindingAwareSDFG, tileUtilization);

//     // Measure execution time
//     stopTimer(&timer);

//     out << "thr " << thr;
//     // out << "thr(" << g->getName() << ") = " << thr << endl;
//     out << "\nrun_time ";
//     printTimer(out, &timer);
//     out << endl;

//     // Cleanup
//     delete bindingAwareSDFG;
//     delete platformGraph;
// }

/**
 * analyzeSDFG ()
 * The function analyzes the SDF graph.
 */
// void analyzeSDFG(ostream &out)
// {
//     TimedSDFgraph *sdfGraph;
//     CNode *sdfNode, *sdfPropertiesNode;

//     // Find sdf graph in XML structure
//     sdfNode = CGetChildNode(settings.xmlAppGraph, "sdf");
//     if (sdfNode == NULL)
//         throw CException("Invalid xml file - missing 'sdf' node");
//     sdfPropertiesNode = CGetChildNode(settings.xmlAppGraph, "sdfProperties");

//     // Construction SDF graph model
//     sdfGraph = new TimedSDFgraph();
//     sdfGraph->construct(sdfNode, sdfPropertiesNode);

//     // The actual analysis...
//     analyzeSDFG(sdfGraph, out);

//     // Cleanup
//     delete sdfGraph;
// }

/**
 * main ()
 * It does none of the hard work, but it is very needed...
 */
 int main(int argc, char **argv)
 {
    int exit_status = 0;
    ofstream out;

    try
    {
        // Initialize the program
        initSettings(argc, argv);

        // Set output stream
        // if (!settings.outputFile.empty())
        //     out.open(settings.outputFile.c_str());
        // else
        //     ((ostream&)(out)).rdbuf(cout.rdbuf());

        // Perform requested actions
        TimedSDFgraph *sdfGraph;
        CNode *sdfNode, *sdfPropertiesNode;
        PlatformGraph *platformGraph;

        // Find sdf graph in XML structure
        sdfNode = CGetChildNode(settings.xmlAppGraph, "sdf");
        if (sdfNode == NULL)
            throw CException("Invalid xml file - missing 'sdf' node");
        sdfPropertiesNode = CGetChildNode(settings.xmlAppGraph, "sdfProperties");

        // Create a platform graph
        if (settings.xmlArchGraph == NULL)
            throw CException("No architectureGraph given.");
        platformGraph = constructPlatformGraph(settings.xmlArchGraph);

        // Construction SDF graph model
        sdfGraph = new TimedSDFgraph();
        sdfGraph->construct(sdfNode, sdfPropertiesNode);

        setMappingPlatformGraph(platformGraph, sdfGraph, settings.xmlMapping);

        SDFconvertToHTML converter;
        converter.setSDFgraph(sdfGraph);
        converter.setPlatformGraph(platformGraph);
        // converter.convert(CString(""));

        ofstream dotGraph("sdf_graph.dot");
        converter.outputSDFgraphAsDot(sdfGraph, dotGraph);
        dotGraph.close();

        dotGraph.open("platform_graph.dot");
        converter.outputPlatformGraphAsDot(platformGraph, dotGraph);
        dotGraph.close();

        dotGraph.open("mapping_graph.dot");
        converter.outputPlatformMappingAsDot(sdfGraph, platformGraph, dotGraph);
        dotGraph.close();

        delete platformGraph;
        delete sdfGraph;
    }
    catch (CException &e)
    {
        cerr << e;
        exit_status = 1;
    }

    xmlFreeDoc(settings.graphDoc);
    return exit_status;
}
